import axios from 'axios';

export const titleRoutes = {
  'index': 'Главная',
  'selections': 'Подборки',
  'selections-id': '',
  'showroom-id': '',
  'category-all': '',
  'profile': 'Профиль',
  'profile-comments': 'Комментарии',
  'profile-favorite': 'Избранное',
  'profile-add-showroom': 'Добавить шоурум',
}
class Category {
  constructor(category, path, parentId = 0) {
    this.alt = category.data.alt;
    this.description = category.data.description
    this.detail_img = category.data.detail_img
    this.grid_img = category.data.grid_img
    this.name = category.data.name
    this.recommend_img = category.data.recommend_img
    this.title = category.data.title
    this.id = category.id
    this.slug = category.data.slug
    this.path = [path, this.slug].join('/');
    this.children = category.children ? category.children.map(category => new Category(category, this.path, parentId)) : []
    this.parentId = parentId
  }
}
/**
 * Рекурсивный обход категорий, для создания плоской структуры
 * @param {Category[]} cats
 * @param {String} path путь до категории
 * @param {Number} parentId Идентификатор родительской категории
 * @returns {Category[]}
 */
const recursive = (cats) => {
  let categories = {};
  cats.forEach(category => {
    categories[category.slug] = category
    if (category.children && category.children.length) {
      categories = { ...categories, ...recursive(category.children) };
    }
  });
  return categories;
};

export default async () => {
  const filters = (await axios.get(process.env.API_URL + '/get-filter/')).data;
  const categories = (await axios.get(process.env.API_URL + '/categories/')).data.map(category => new Category(category))
  const mapCategories = recursive(categories);
  return {
    server: {
      host: '0.0.0.0',
    },
    router: {
      middleware: "clear",
      extendRoutes(routes) {
        routes.forEach((route) => (route.meta = titleRoutes[route.name]))
      }
    },
    head: {
      title: 'Главная',
      htmlAttrs: {
        lang: 'en',
      },
      meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { hid: 'description', name: 'description', content: '' },
        { name: 'format-detection', content: 'telephone=no' },
      ],
      link: [
        { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
        { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.1.3/css/bootstrap.min.css' }
      ],
    },
    css: ["@/assets/style/styles.less"],
    styleResources: {
      less: ["@/assets/style/vars/mixins.less", "@/assets/style/vars/vars.less"]
    },
    plugins: [
      "@/plugins/globals",
      "@/plugins/axios",
    ],
    components: true,
    buildModules: [
      '@nuxtjs/eslint-module',
      '@nuxtjs/style-resources',
      ['@nuxtjs/google-fonts', {
        preload: true,
        families: {
          Roboto: [400, 700],
          'Playfair+Display': [400, 700],
          Cinzel: [400]
        }
      }],
    ],
    modules: [
      '@nuxtjs/axios',
      '@nuxtjs/dayjs',
      '@nuxtjs/auth-next',
      '@nuxtjs/toast',
      '@nuxtjs/sitemap',
      '@nuxtjs/robots',
      ['vue-yandex-maps/nuxt', {
        apiKey: process.env.YANDEX_MAP_API_KEY,
        lang: 'ru_RU',
        coordorder: 'latlong',
        enterprise: false,
        version: '2.1'
      }]
    ],
    auth: {
      strategies: {
        local: {
          autoRefresh: {
            enable: true,
          },
          token: {
            property: 'auth_token',
            maxAge: 1800,
          },
          user: {
            property: "",
          },
          endpoints: {
            login: {
              url: '/auth/token/login/',
              method: 'post',
              propertyName: 'auth_token',
            },
            logout: {
              url: "/auth/token/logout/",
              method: "post",
            },
            user: {
              url: "/auth/users/me/",
              method: "get"
            },
          },
        },
      },
      redirect: {
        login: '/',
        logout: false,
        home: false,
        callback: false
      }
    },
    robots: {
      UserAgent: "*",
      Allow: "/",
      Disallow: ["/profile", "/profile/*"],
      Sitemap: `${process.env.HOST}/sitemap.xml`,
      Host: process.env.HOST
    },
    axios: {
      baseURL: '/',
    },
    dayjs: {
      locales: ['en', 'ru'],
      defaultLocale: 'ru',
    },
    build: {
      extractCSS: true,
    },
    publicRuntimeConfig: {
      filters,
      categories,
      mapCategories
    }
  };
};
