/**
 * Creates a copy of the constants object for use inside the module
 * @param {String} namespace The namespace of the Vuex module
 * @param {VuexTypes} types An object with constants written together with the namespace
 * @return {VuexTypes} An object with constants, without a namespace entry
 */
function makeItPrivate(namespace, types) {
  /** @type {VuexTypes} */
  const _private = {};
  for (const key in types) {
    _private[key] = {};
    for (const type in types[key]) {
      _private[key][type] = types[key][type].replace(`${namespace}/`, '');
    }
  }
  return _private;
}

/** @type {VuexTypes} */
export const TypesPopup = {
  mutations: {
    OPEN_POPUP: "popup/open",
    CLOSE_POPUP: "popup/close",
  },
};

/** @type {VuexTypes} */
export const TypesGlobal = {
  actions: {
    SET_TIME_ERROR: "setError",
    NUXT_SERVER_INIT: "nuxtServerInit",
  },
  mutations: {
    SET_LOADING: "setLoading",
    SET_ERROR: "setError",
    CLEAR_ERROR: "clearError",
    SET_TITLE: "setTitle"
  },
};

/** @type {VuexTypes} */
export const TypesCategories = {
  mutations: {
    SET_CATEGORIES: "categories/set"
  },
  getters: {
    HAS_CATEGORY: "categories/hasCategory"
  }
};

/** @type {VuexTypes} */
export const TypesMap = {
  mutations: {
    SET_COORDS: "map/setCoords"
  },
  actions: {
    FETCH: "map/fetchCoords",
  },
  getters: {
    GET_PLACEMARKS: "map/getPlacemarks",
  }
};

/** @type {VuexTypes} */
export const _Popup = makeItPrivate('popup', TypesPopup);
/** @type {VuexTypes} */
export const _Categories = makeItPrivate('categories', TypesCategories);
/** @type {VuexTypes} */
export const _Map = makeItPrivate('map', TypesMap);