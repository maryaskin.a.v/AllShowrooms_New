/** @module Types */

/** @namespace Categories */
/**
 * @typedef {Object} Category
 * @property {Number} id Идентификатор
 * @property {String} name Название
 * @property {String} title Кодовое название
 * @property {Category[]} children
 */
/** @namespace Map */
/**
 * Даннные метки на карте
 * @typedef {Object} Placemark
 * @property {Number} id
 * @property {String} name Имя шоурума
 * @property {Coordinates[]} coords Координаты, необходимая длинна массива: 2
 * @property {String} hint-content Подпись при наведении на метку
 */
/**
 * @typedef {Object} Coordinates
 * @property {string} address Адрес шоурума
 * @property {string[]} domains Коды городов в которых есть шоурум
 * @property {Number} latitude
 * @property {Number} longitude
 */

/** @namespace Vuex */
/**
 * @typedef VuexTypes
 * @property {!Object.<string, string>} mutations
 * @property {!Object.<string, string>} actions
 * @property {!Object.<string, string>} getters
 */
/** @namespace Products */
/**
 * @typedef Showroom
 * @property {Number} id Идентификатор
 * @property {String[]} addresses адреса
 * @property {String} name Имя
 * @property {String} title Кодовое имя
 * @property {String} description Описание
 * @property {Number | null} user_star Проставлен рейтинг текущим пользователем?
 * @property {String} average_star Средний рейтинг шоурума
 * @property {Number} saved_number Количество пользователей, которые добавили в избранное
 * @property {Boolean} saved_user Добавлено в избранное текущим пользователем?
 * @property {String} grid_img Изображение в общей выборке
 * @property {String} recommend_img Изображение ...
 * @property {Number} comments_number Количество комментариев
 * @property {Image[]} showroom_detail_images Список изображений шоурума
 * @property {Coordinates[]} coords Координаты шоурума
 */
/**
 * @typedef ShowroomDetails
 * @property {Number} id Идентификатор
 * @property {String} name Имя
 * @property {String} title Кодовое имя
 * @property {String} description Описание
 * @property {String} description_for_address Описание рядом с адресом
 * @property {Number | null} user_star Проставлен рейтинг текущим пользователем?
 * @property {String} average_star Средний рейтинг шоурума
 * @property {Number} saved_number Количество пользователей, которые добавили в избранное
 * @property {Boolean} saved_user Добавлено в избранное текущим пользователем?
 * @property {String} grid_img Изображение в общей выборке
 * @property {String} recommend_img Изображение ...
 * @property {Number} comments_number Количество комментариев
 * @property {Image[]} showroom_detail_images Список изображений шоурума
 * @property {Coordinates[]} coords Координаты шоурума
 * 
 * @property {String[]} addresses Список адресов шоурумов
 * @property {String[]} domains Список городов шоурумов {@link Placemark 0 элемент сырой метки}
 * @property {Object[]} showroom_social Список социальных сетей шоурума
 * @property {Object[]} showroom_phone Список номеров шоурума
  * @property {String} showroom_phone.phone_number 
  * @property {String} showroom_phone.link
  * @property {String} showroom_phone.social_name Имя соц.сети
  * @property {String} showroom_phone.social_icon Иконка соц.сети
 * @property {ShowroomDetails[]} showroom_children Дочерние шоурумы
 * @property {String} header_img
 * @property {String} detail_img
 * @property {String} link Официальная страница
 * @property {Number} parent Родительская категория шоурума
 * @property {Boolean} is_vintage Флаг
 * @property {Boolean} is_draft Флаг
 * @property {Number[]} categories Список идентификаторов категорий
 * @property {Date} time_published Родительская категория шоурума
 */
/**
 * @typedef ShowroomItem
 * @property {String} name Имя
 * @property {String} title Кодовое имя
 * @property {String} description Описание
 * @property {String} link Ссылка
 * @property {String} grid_img Изображение в общей выборке
 * @property {String} recommend_img Изображение ...
 */
/**
 * @typedef ShowroomComment
 * @property {number} id
 * @property {string} name
 * @property {string} recommend_img
 * @property {string} title
 */
/** @namespace Selections */
/**
 * @typedef SelectionItem
 * @property {String} name Имя
 * @property {String} title Кодовое имя
 * @property {?String} grid_img Изображение в общей выборке
 * @property {?String} recommend_img Изображение ...
 */
/**
 * @typedef SelectionInfo
 * @property {!Number} id Идентификатор выборки
 * @property {String} name Имя выборки
 * @property {String} title Кодовое имя выборки
 * @property {String} description Описание выборки
 * @property {?String} detail_img
 * @property {?String} grid_img
 * @property {?String} header_img
 * @property {?String} recommend_img
 * @property {ShowroomItem[]} items Шоурумы выборки
 * @property {Showroom} showrooms Шоурумы выборки
 */

/**
 * @typedef SelectionHeader
 * @property {String} name
 * @property {String} title
 * @property {String?} header_img
 */

/** @namespace Comments */
/**
 * @typedef UserComment
 * @property {String} user_name Никнейм пользователя
 * @property {?String} avatar Аватар пользователя
 * @property {String} text Содержимое комментария
 * @property {String} time_published Дата в формате UTC
 * @property {?Number} parent ???
 * @property {Number} showroom Идентификатор {@link Showroom шоурума}
 * @property {?Number} selection Идентификатор {@link Selection выборки}
 * @property {Image[]} comment_images Изображения, прикреплённые к комментарию
 * @property {Object} reactions Реакции пользователей
 * @property {number} reactions.likes Положительные реакции пользователей
 * @property {number} reactions.dislikes Негативные реакции пользователей
 * @property {UserComment[]} children Ответы на комментарий
 */

/**
 * @typedef Image
 * @property {?String} image
 * @property {?String} image_thumbnail
 */

/**
 * @typedef User
 * @property {string} id
 * @property {string} email
 * @property {string} username
 * @property {string?} avatar
 * @property {boolean} comments_confirm
 * @property {boolean} news_confirm
 */

/** @namespace Responses */
/** 
 * @typedef Pagination
 * @property {Number} count
 * @property {Number} count_pages
 * @property {Object} links
 * @property {?String} links.next
 * @property {?String} links.previous
 */
/** @typedef {Pagination & {results: UserComment[]}} ResponseComments */
/** @typedef {Pagination & {results: SelectionItem[]}} ResponseSelectionList */
/** @typedef {Pagination & {results: Showroom[]}} ResponseProductItems */
/** @typedef {Pagination & {results: ShowroomItem[]}} ResponseProductList */
/** @typedef {Pagination & {results: ShowroomDetails[]}} ResponseShowroomsList */
/** @typedef {Pagination & {results: ShowroomComment[]}} ResponseShowroomsCommentsList */