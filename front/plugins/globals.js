import Vue from "vue";


import Vuelidate from "vuelidate"
import VueSlickCarousel from 'vue-slick-carousel';
import NuxtJsonld from 'nuxt-jsonld';

import RelatedContent from "@/components/globals/RelatedContent.vue";
import BasePopup from '@/components/globals/BasePopup.vue';
import CategoryList from '@/components/globals/CategoryList.vue';
import BaseAvatar from "@/components/globals/BaseAvatar.vue";
import BasePagination from '@/components/globals/BasePagination.vue';
import BaseDropdown from '@/components/globals/BaseDropdown.vue';
import BaseBreadcrumbs from '@/components/globals/BaseBreadcrumbs.vue';
import BaseRating from "@/components/globals/BaseRating.vue";
import BaseComment from "@/components/globals/BaseComment.vue";
import BaseTags from "@/components/globals/BaseTags.vue";
import BaseInput from "@/components/globals/BaseInput.vue";
import('vue-slick-carousel/dist/vue-slick-carousel.css');

const globalComponents = {
  BaseAvatar,
  BaseBreadcrumbs,
  BaseComment,
  BaseDropdown,
  BasePopup,
  BasePagination,
  BaseRating,
  BaseTags,
  BaseInput,
  CategoryList,
  VueSlickCarousel,
  RelatedContent,
};

Object
  .entries(globalComponents)
  .forEach(([name, component]) => {
    Vue.component(name, component);
  });

Vue.use(Vuelidate)
Vue.use(NuxtJsonld)