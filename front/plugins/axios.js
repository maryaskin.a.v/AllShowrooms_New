import { setupCache } from "axios-cache-adapter";
import { TypesGlobal } from "@/constants/store.types";

const cache = setupCache({
  maxAge: 15 * 60 * 1000,
  exclude: {
    query: false
  }
})
const isDev = process.env.NODE_ENV === "development";

const wrapInstance = (instance, bearer) => {
  if (bearer) {
    instance.setToken(bearer, '');
  }
  instance.onRequest(config => {
    config.adapter = cache.adapter
    if (isDev) {
      if (config.baseURL === "/") {
        config.url = "/api" + config.url;
      }
      console.log(config.baseURL, config.url);
    }
    return config
  })
  return instance;
};

export default function ({ $axios, error: nuxtError, store }) {
  wrapInstance($axios);

  $axios.onError((error) => {
    const statusCode = parseInt(error.response && error.response.status)
    if (statusCode === 404) {
      nuxtError({
        statusCode,
        message: error.message
      });
    }
    store.dispatch(TypesGlobal.actions.SET_TIME_ERROR, { error: statusCode === 500 ? 'Ошибка сервера!' : error.response.data })
    return Promise.reject(error);
  })
  $axios.onRequest((req) => {
    if (req.headers.Authorization) {
      req.headers.Authorization = req.headers.Authorization.replace('Bearer', 'Token')
    }
    store.commit(TypesGlobal.mutations.SET_LOADING, true)
    return Promise.resolve(req)
  })
  $axios.onResponse((res) => {
    store.commit(TypesGlobal.mutations.SET_LOADING, false)
    return Promise.resolve(res)
  });
  $axios.onResponseError((res) => {
    store.commit(TypesGlobal.mutations.SET_LOADING, false)
    return Promise.reject(res)
  });
}
