import { TypesGlobal } from "~/constants/store.types";

export default function ({ store, route }) {
  store.commit(TypesGlobal.mutations.SET_TITLE, route.name);
}