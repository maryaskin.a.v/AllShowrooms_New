/** @module Utils */
/**
 * @param {Object.<string, string>} object 
 * @returns Объект без свойств с пустыми значениями
 * 
 * @example removeEmptyParams({foo: '1', bar: ''}) === {foo: '1'}
 */
function removeEmptyValues(object) {
  if (!object) return undefined;
  return Object.fromEntries(
    Object.entries(object).filter(([_, v]) => v?.length || typeof v === "number")
  );
}
/**
 * @param {Object.<string, string>} query 
 * @returns {{query: Object.<string, string>}}
 */
export function mergeQuery(query) {
  return {
    query: removeEmptyValues(Object.assign({}, this.$route.query, query))
  };
}

export function createURLSearch(query) {
  if (!query) return '';
  const url = new URLSearchParams(
    Object.entries(removeEmptyValues(query))
      .flatMap(([key, items]) => {
        if (typeof items === 'object') {
          return items.map(val => `${key}=${val}`);
        } else {
          return `${key}=${items}`;
        }
      })
      .join('&')
  ).toString();
  return url;
}

export function Defer(count = 10) {
  return {
    data() {
      return {
        displayPriority: 0
      }
    },

    mounted() {
      this.runDisplayPriority()
    },

    methods: {
      runDisplayPriority() {
        const step = () => {
          requestAnimationFrame(() => {
            this.displayPriority++
            if (this.displayPriority < count) {
              step()
            }
          })
        }
        step()
      },

      defer(priority) {
        return this.displayPriority >= priority
      }
    }
  }
}

/**
 * Функция отложенного вызова переданной функции
 * @param {Function} callee Вызываемая функция
 * @param {number} timeoutMs Через какое время совершить вызов
 * @returns Вызов функции
 */
export function debounce(callee, timeoutMs) {
  let timer;
  return function perform(...args) {
    if (timer) {
      clearTimeout(timer);
    }
    timer = setTimeout(() => callee.apply(this, args), timeoutMs);
  };
}


/**
 * @param {Object} object 
 * @returns {FormData}
 */
export function createFormData(object) {
  const formData = new FormData();
  for (const property in object) {
    if (Array.isArray(object[property])) {
      for (const item of object[property]) {
        formData.append(property, item)
      }
    } else if (object[property] !== null) {
      formData.append(property, object[property])
    }
  }
  return formData
}