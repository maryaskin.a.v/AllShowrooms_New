import { mergeQuery } from './utils';

export const fetch = {
  data() {
    return {
      /** @type {ResponseProductList} */
      productItems: {
        count_pages: 1,
        count: 0,
        links: {
          next: null,
          previous: null,
        },
        results: [],
      },
    };
  },
  watch: {
    '$route.query': {
      async handler(val) {
        /** @type {ResponseProductItems} */
        this.productItems = await this.$store.dispatch('products/fetchProducts', val);
      },
      deep: true,
      immediate: true,
    },
  },
  methods: {
    appendQuery(params) {
      this.$router.push(mergeQuery.bind(this, params)());
    }
  }
};

export const reactions = {
  methods: {
    async onRate(star, id) {
      await this.$store.dispatch("products/onRate", { showroom: id, star })
      this.$emit("rate", id)
    },
    async onSave(id) {
      await this.$store.dispatch("products/onSave", { showroom: id })
      this.$emit('save', id)
    },
  }
}