import { _Popup } from "~/constants/store.types";

/**
 * @typedef State
 * @property {?String} currentPopup ключевое имя текущего открытого попап-окна
 */
export const state = () => ({
  currentPopup: null,
});

export const mutations = {
  [_Popup.mutations.OPEN_POPUP]:
    /**
     * @param {State} state 
     * @param {?String} popup 
     */
    (state, popup) => {
      state.currentPopup = popup;
    },
  [_Popup.mutations.CLOSE_POPUP]: /** @param {State} */ (state) => {
    state.currentPopup = null;
  }
};