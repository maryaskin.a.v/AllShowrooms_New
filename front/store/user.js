import { createFormData, createURLSearch } from '@/mixins/utils';
export const actions = {
  /**
   * Запрос шоурумов, в которых есть комментарии пользователя
   * @param {Object.<string, string>} query
   * @returns {Promise<ResponseShowroomsCommentsList>}
   */
  fetchShowrooms(_, query) {
    return this.$axios.$get(`/profiles/showrooms/?${createURLSearch(query)}`)
  },
  /**
   * Запрос комментариев пользователя из конкретного шоурума
   * @param {object} payload 
   * @param {number} payload.id 
   * @param {Object.<string, string>} payload.query 
   * @returns {Promise<ResponseComments>}
   */
  fetchShowroomComments(_, { id, query }) {
    return this.$axios.$get(`/profiles/showrooms/${id}/comments/?${createURLSearch(query)}`)
  },
  /**
   * @returns {ResponseProductItems}
   */
  fetchSaved() {
    return this.$axios.$get('/profiles/saved/')
  },
  /**
   * @param {Omit<User, "id">} form 
   * @returns {Promise<User>}
   */
  async changeProfile({ rootState, getters }, form) {
    try {
      const changedUser = await this.$axios.$patch(`/profiles/${rootState.auth.user.id}/`, createFormData(getters.getSymmetricDifference(form)))
      this.$auth.$storage.setState('user', changedUser)
      return changedUser
    } catch {
      return form
    }
  }
};
export const getters = {
  getSymmetricDifference(state, getters, rootState) {
    return (user) => Object.fromEntries(
      Object.entries(user).filter(
        ([field, value]) => rootState.auth.user[field] !== value || field === "email"
      )
    )
  }
}