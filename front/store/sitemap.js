export const state = () => ({
  sitemap: {},
});

const createMap = (sitemap, identifiers = { key: 'id', value: 'slug' }) => {
  const obj = {}
  Object.entries(sitemap).forEach(([route, entities]) => {
    if (!(route in obj)) {
      obj[route] = {}
    }
    entities.forEach((entity) => {
      obj[route][entity[identifiers.key]] = entity[identifiers.value]
    })
  })
  return obj;
}

export const mutations = {
  setSitemap: (state, payload) => {
    state.sitemap = payload;
  }
};

export const getters = {
  slugMap(store) {
    return createMap(store.sitemap, { key: 'slug', value: 'id' })
  },
  idMap(store) {
    return createMap(store.sitemap);
  }
}