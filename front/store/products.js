import { createURLSearch } from '@/mixins/utils';

export const state = () => ({});

export const mutations = {
  addProducts(state, products) {
    products.forEach(product => {
      if (product.id in state) return;
      state[product.id] = product;
    });
  },
  rate(state, { star, showroom }) {
    state[showroom].user_star = star;
  },
  like(state, showroom) {
    state[showroom].saved_user = !state[showroom].saved_user
  }
}
export const actions = {
  onRate({ commit, dispatch }, { showroom, star }) {
    dispatch('authService/doIfAuth', async () => {
      try {
        if (star) {
          await this.$axios.$post("/rating/", { star, showroom })
        } else {
          await this.$axios.$delete("/rating/", { data: { showroom } })
        }
        commit('rate', { star, showroom })
      } catch { }
    }, { root: true })
  },
  onSave({ state, commit, dispatch }, { showroom }) {
    dispatch('authService/doIfAuth', async () => {
      try {
        if (state[showroom].saved_user) {
          await this.$axios.$delete("/saved/", { data: { showroom } })
        } else {
          await this.$axios.$post("/saved/", { showroom })
        }
        commit('like', showroom)
      } catch { }
    }, { root: true })
  },
  async onSearch({ _ }, term) {
    return (await this.$axios.$get(`/showrooms/search/?search=${term}`))
      .results
      .slice(0, 4)
      .map(product => ({ path: product.id, name: product.name }))
  },
  async fetchProducts({ commit }, query) {
    const products = await this.$axios.$get("/showrooms/?" + createURLSearch(query))
    commit('addProducts', products.results)
    return products;
  },
  async fetchSchema({ _ }, showroomId) {
    return await this.$axios.$get(`/showrooms/${showroomId}/structured_data/`)
  },
  async fetchProduct({ commit }, showroom) {
    const product = await this.$axios.$get(`/showrooms/${showroom}/`);
    commit('addProducts', [product])
    return product;
  },
  async fetchSaved({ commit, dispatch }) {
    const products = await dispatch('user/fetchSaved', {}, { root: true })
    products.results.forEach(product => {
      product.saved_user = true
    })
    commit('addProducts', products.results)
    return products
  },
  async fetchSelection({ commit }, id) {
    const selection = await this.$axios.$get(`/selections/${id}/`)
    commit('addProducts', selection.showrooms);
    return selection;
  },
};

export const getters = {
  savedProducts(store) {
    return Object.values(store).filter(product => product.saved_user)
  }
}