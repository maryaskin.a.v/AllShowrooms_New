import { TypesPopup } from '@/constants/store.types';
import { HEADER_AUTH } from "@/constants/popups"

/**
 * @typedef ResponseRegister
 * @property {string} email
 * @property {string} username
 * @property {number} id
 */

/**
 * @typedef ResponseLogin
 * @property {string} auth_token
 */
export const actions = {
  /**
   * Активация аккаунта, через подтверждение почты по ссылке
   * @param {Object} data 
   * @param {string} data.uid
   * @param {string} data.token
   */
  activate(_, data) {
    return this.$axios.$post('/auth/users/activation/', data)
  },
  /**
   * Регистрация аккаунта
   * @param {Object} form 
   * @param {boolean} form.agreePolicy
   * @param {string} form.email
   * @param {string} form.password
   * @param {string} form.username
   * @returns {Promise<ResponseRegister>}
   */
  register(_, form) {
    return this.$axios.$post('/auth/users/', form)
  },
  /**
   * Восстановление аккаунта
   * @param {Object} form 
   * @param {string} form.email
   * @returns {Promise<void>}
   */
  recover(_, form) {
    return this.$axios.$post('/profiles/reset_password/', form)
  },
  /**
   * Вход в аккаунт
   * @param {Object} form 
   * @param {string} form.username
   * @param {string} form.password
   * @returns {Promise<ResponseLogin>}
   */
  login(_, form) {
    return this.$auth.loginWith('local', { data: form })
  },
  /**
   * Выход из аккаунта
   * @param {string} to
   */
  logout(_, to = '/') {
    return this.$auth.logout().then(() => this.$router.push(to))
  },
  doIfAuth({ commit, rootState }, callback) {
    if (rootState.auth.loggedIn) {
      return callback();
    } else {
      commit(TypesPopup.mutations.OPEN_POPUP, HEADER_AUTH, { root: true })
    }
  }
};