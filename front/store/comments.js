import { createFormData } from '@/mixins/utils';/**
* @typedef {Object} ResponseCreateComments
* @property {Image[]} comment_images
* @property {number} id
* @property {number?} parent
* @property {number} showroom
* @property {string} text
*/
export const actions = {
  /**
   * @param {Object} form
   * @param {string} form.text
   * @param {number} form.showroom
   * @param {number?} form.parent
   * @param {Image[]} form.uploaded_images
   * @returns {Promise<ResponseCreateComments>}
   */
  createComment(_, form) {
    return this.$axios.$post('/comments/showroom/', createFormData(form))
  },
  /**
   * @returns {UserComment[]}
   */
  fetch(_, { query, id }) {
    const pageQuery = query.page
      ? '?page=' + query.page
      : ''
    return new Promise(resolve => {
      this.$axios.$get(
        `/comments/showroom/${id}/${pageQuery}`,
        {
          cache: {
            exclude: {
              query: true,
              methods: ['get'],
            },
          },
        }
      ).then(comments => {
        // Трансформация ответа под нужный формат
        resolve({
          ...comments,
          results: comments.results.map(comment => {
            const reactions = {
              likes: comment.likes,
              dislikes: comment.dislikes
            }
            delete comment.likes
            delete comment.dislikes
            return {
              ...comment,
              reactions
            }
          })
        })
      })
    })
  },
  /**
   * Отправка на сервер реакции пользователя на конкретный комментарий
   * @param {Object} comment
   * @param {number} comment.id Идентификатор комментария
   * @param {"like"|"dislike"} comment.reaction Реакция
   */
  setCommentReaction({ dispatch }, { reaction, id }) {
    return dispatch('authService/doIfAuth', () => this.$axios.$post(`/comments/${reaction}/${id}/`), { root: true })
  }
};