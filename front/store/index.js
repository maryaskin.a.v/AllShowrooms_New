import { TypesCategories, TypesMap } from "@/constants/store.types";
import { TypesGlobal } from "~/constants/store.types";
import { titleRoutes } from "@/nuxt.config";

/**
 * @typedef State
 * @property {Boolean} loading Состояние загрузки
 */
/** @type {State} */
export const state = () => ({
  loading: false,
  title: ""
});

export const mutations = {
  [TypesGlobal.mutations.CLEAR_ERROR](state) {
    state.error = null;
  },
  [TypesGlobal.mutations.SET_LOADING]:
    /**
     * Установка состояния загрузки
     * @param {State} state 
     * @param {Boolean} payload 
     */
    (state, payload) => {
      state.loading = payload;
    },
  [TypesGlobal.mutations.SET_TITLE](state, routeName) {
    state.title = titleRoutes[routeName] || routeName
  }
};
export const actions = {
  async [TypesGlobal.actions.NUXT_SERVER_INIT]({ dispatch, commit }, { $config }) {
    await dispatch(TypesMap.actions.FETCH);
    commit(TypesCategories.mutations.SET_CATEGORIES, { categories: $config.categories, mapCategories: $config.mapCategories });
    commit('sitemap/setSitemap', $config.sitemaps)
  },
  [TypesGlobal.actions.SET_TIME_ERROR](_, { error, time = 3000 }) {
    const message = typeof error === "string" ? { error } : error;
    Object.keys(message).forEach((field) => {
      const error = Array.isArray(message[field]) ? message[field].join('<br/>') : message[field]
      this.$toast?.error(`${field}: ${error}`, {
        duration: time
      })
    })
  }
};