import { _Map } from "@/constants/store.types";

/**
 * @typedef State
 * @property {Object.<string, Placemark>} coords Список соотношений города к шоурумам с координатами
 */
/** @type {State} */
export const state = () => ({
  coords: {}
});

export const mutations = {
  [_Map.mutations.SET_COORDS]:
    /**
     * @param {State} state 
     * @param {Placemark[]} payload 
     */
    (state, payload) => {
      payload.forEach((placemark) => {
        state.coords[placemark.id] = new Placemark(placemark)
      })
    }
};
export const actions = {
  async [_Map.actions.FETCH]({ commit }) {
    commit(_Map.mutations.SET_COORDS, await this.$axios.$get('/showrooms/geo/'));
  }
};

export const getters = {
  [_Map.getters.GET_PLACEMARKS]:
    /**
     * @param {State} state 
     */
    (state) =>
      /**
       * @param {Showroom[]} items
       * @param {string[]} domains
       * @returns {Placemark[]}
       */
      (showrooms, domains = []) => {
        return showrooms.flatMap(({ id }) => {
          return domains.length ? state.coords[id].markers.filter(marker => domains.includes(marker.domain.id.toString())) : state.coords[id].markers
        })
      }
};
class Marker {
  // eslint-disable-next-line camelcase
  constructor({ name, address, latitude, longitude, domain_id, domain }) {
    this.name = name || address;
    this['hint-address'] = address;
    this.coords = [latitude, longitude];
    this.id = this.coords.join('');
    this.domain = {
      name: domain,
      // eslint-disable-next-line camelcase
      id: domain_id
    }
  }
}

class Placemark {
  constructor({ id, name, coords }) {
    this.id = id;
    this.name = name;
    this.markers = coords.map(coordinate => new Marker(coordinate))
  }
}