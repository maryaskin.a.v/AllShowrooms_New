import { _Categories } from "~/constants/store.types";
import "@/constants/typedef"
/**
 * @typedef State
 * @property {Category[]} categories
 * @property {Object.<number, Category>} map // Плоская структура категорий, все категории на одном уровне
 */
/** @type {State} */
export const state = () => ({
  categories: [],
  map: {},
});

export const mutations = {
  /**
   * @param {State} state
   * @param {{mapCategories: Category[], categories: Object.<number, Category> }} categories 
   */
  [_Categories.mutations.SET_CATEGORIES]: (state, categories) => {
    state.categories = categories.categories
    state.map = categories.mapCategories
  }
}

export const getters = {
  [_Categories.getters.HAS_CATEGORY]: (state) => (id) => id in state.map
};