ymaps.ready(init);


async function get_coords(){
    let resp = await fetch('/coords/');
    let coords = await resp.json();
    console.log(coords)

    // Getting latitudes and longitudes for the map center
    let latitudes = 0;
    let longitudes = 0;
    let length_arr = 0

    for (let city in coords) {
        length_arr += coords[city].length
        for (let arr of coords[city]) {
            latitudes += Number(arr[1]);
            longitudes += Number(arr[2]);
        }
    };

    let average_latitude = (latitudes / length_arr).toFixed(2);
    let average_longitude = (longitudes / length_arr).toFixed(2);

    return [average_latitude, average_longitude, coords]
}


async function init(){
    // Getting coordinates from view. 'domain__city', 'addresses__name', 'latitude', 'longitude'
    [average_latitude, average_longitude, coords] = await get_coords()
    console.log('test', average_latitude, average_longitude)
    // Creating a map
    let myMap = new ymaps.Map("map", {
        // Координаты центра карты.
        // Порядок по умолчанию: «широта, долгота».
        // center: [55.76, 37.64],
        center: [average_latitude, average_longitude],
        // Уровень масштабирования. Допустимые значения:
        // от 0 (весь мир) до 19.
        zoom: 7
    });

    let myCollection = new ymaps.GeoObjectCollection({}, {
         preset: 'islands#redIcon', //все метки красные
         draggable: true // и их можно перемещать
        });

    let GeoObj = []

    for (let city in coords) {
        for (let j of coords[city]) {
            GeoObj.push(new ymaps.GeoObject({
                geometry: {
                    type: "Point",
                    coordinates: [j[1], j[2]]
                }
            }))
        }
    }

    let myClusterer = new ymaps.Clusterer();
    myClusterer.add(GeoObj);
    myMap.geoObjects.add(myClusterer);

}
