"""allshowrooms_new URL Configuration
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from .yasg import urlpatterns as doc_urls


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('auth/', include('djoser.urls')),
    path('auth/', include('djoser.urls.authtoken')),
    path('', include('showrooms.urls')),
    path('profiles/', include('profiles.urls')),
    path('selections/', include('selections.urls')),
    path('comments/', include('comments.urls')),
]


urlpatterns += doc_urls


urlpatterns += [path('silk/', include('silk.urls', namespace='silk'))]


if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

