#!/bin/bash

set -euo pipefail

python manage.py collectstatic --no-input
python manage.py makemigrations
python manage.py migrate
gunicorn -b 0.0.0.0:8000 config.wsgi:application