from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views


urlpatterns = format_suffix_patterns([
    path('showrooms/', views.ShowroomViewSet.as_view({'get': 'list'})),
    path('showrooms/search/', views.ShowroomViewSet.as_view({
        'get': 'search_showroom'})),
    path('showrooms/geo/', views.ShowroomViewSet.as_view({
        'get': 'get_geo_data'})),
    path('showrooms/<int:pk>/', views.ShowroomViewSet.as_view({
        'get': 'retrieve'})),
    path("showrooms/<int:pk>/bestitems/", views.BestItemsView.as_view()),
    path('showrooms/<int:pk>/structured_data/',
         views.ShowroomStructuredData.as_view()),
    path('categories/', views.CategoryMenuView.as_view()),
    path('categories/<int:pk>/', views.CategoryDetailView.as_view()),
    path('categories/recommendations/',
         views.CategoryRecommendationsView.as_view()),
    path('showrooms/recommendations/',
         views.ShowroomRecommendationsView.as_view()),
    path("rating/", views.AddStarRatingViewSet.as_view({'post': 'create',
                                                        'delete': 'destroy',
                                                        'patch': 'update'})),
    path('saved/', views.SaveShowroomViewSet.as_view({'post': 'create',
                                                      'delete': 'destroy',
                                                      'patch': 'update'})),
    path('get-filter/', views.GetFilterView.as_view()),
    path('sitemap/', views.SitemapDataView.as_view()),
])
