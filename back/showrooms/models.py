from django.utils import timezone
from django.db import models
from django.core.validators import (
    RegexValidator, MaxValueValidator, MinValueValidator)
from django.db.models.signals import m2m_changed
from django.dispatch import receiver
from django.db.models import Avg
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.validators import FileExtensionValidator
from django.contrib.postgres.expressions import ArraySubquery
from django.core.exceptions import ValidationError
from treebeard.mp_tree import MP_Node

from common.models import ImageAbstract
from common.service import (
    get_path_image, ImageDimensionsValidator, make_thumbnail)
from .service import get_path_icon, get_path_showroom_image


class Category(ImageAbstract, MP_Node):
    """Categories of showrooms"""
    name = models.CharField(max_length=200, unique=True,
                            help_text='Category name in Russian')
    title = models.CharField(max_length=200, help_text='Title for SEO')
    slug = models.SlugField(unique=True)
    description = models.TextField(max_length=800)
    detail_img = models.ImageField(
        upload_to=get_path_image,
        help_text=f"Required size: {settings.IMG_SIZE['category_detail']}",
        validators=[FileExtensionValidator(allowed_extensions=['png']),
                    ImageDimensionsValidator('category_detail')])

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Categories"


class Domain(models.Model):
    """Domains for showrooms"""
    city = models.CharField(max_length=128, help_text="City name in Russian")
    prefix = models.CharField(
        max_length=128, blank=True, unique=True,
        help_text="City prefix for subdomain: moscow. spb, etc")
    domain = models.CharField(max_length=128, unique=True,
                              default=settings.BASE_DOMAIN)

    def save(self, *args, **kwargs):
        if (not self.prefix.startswith("http://") or
                not self.prefix.startswith("https://")):
            self.prefix = "http://" + self.prefix
        if self.prefix == "http://" or self.prefix == "https://":
            self.domain = self.prefix + self.domain
            super(Domain, self).save(*args, **kwargs)
        else:
            self.domain = self.prefix + "." + self.domain
            super(Domain, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.city} - {self.domain}'


class ShowroomAnnotation(models.Manager):
    """Showrooms with ratings, comments, and saved annotations"""

    def get_annotations(self, user_id, subquery):
        return self.annotate(
            average_star=Avg(models.F('ratings__star'))
        ).annotate(
            saved_number=models.Count(
                'saved',
                distinct=True
            )
        ).annotate(
            saved_user=models.Count(
                models.F('saved'), filter=models.Q(saved__saved_by_id=user_id)
            )
        ).annotate(
            comments_number=models.Count(
                'showroom_comments',
                distinct=True
            )
        ).annotate(
            user_star=models.F(
                'ratings__star',
            )
        ).annotate(
            coords=ArraySubquery(subquery)
        )


class Showroom(ImageAbstract):
    """Showrooms"""
    name = models.CharField(max_length=200, unique=True,
                            help_text='Showroom official name')
    title = models.CharField(max_length=200,
                             help_text='Title for SEO')
    slug = models.SlugField(unique=True)
    description = models.TextField(max_length=800)
    description_for_address = models.TextField(
        max_length=800,
        help_text='Description near the map block: '
                  'https://gyazo.com/b5d34a326115ca0e344a0e080f33f3eb')
    link = models.URLField(max_length=200, blank=True)
    is_vintage = models.BooleanField(default=False)
    is_draft = models.BooleanField(default=True)
    time_published = models.DateTimeField()
    parent = models.ForeignKey(
        'Showroom', null=True, blank=True, on_delete=models.SET_NULL,
        help_text='Related showrooms',
        related_name='showroom_children')
    categories = models.ManyToManyField(
        'Category', related_name='showrooms_in_category')
    domains = models.ManyToManyField('Domain', related_name='domains')
    addresses = models.ManyToManyField(
        'Address', blank=True, related_name='showrooms_in_addresses')
    objects = ShowroomAnnotation()

    def save(self, *args, **kwargs):
        if not self.time_published and not self.is_draft:
            self.time_published = timezone.now()
        super(Showroom, self).save(*args, **kwargs)

    def __str__(self):
        return self.name


@receiver(m2m_changed, sender=Showroom.domains.through)
def set_default_domain(
        signal, sender, instance: Showroom, action, pk_set, **kwargs):
    """Add default domain (All cities) to every showroom"""
    if action == "post_add":
        domain = Domain.objects.get(city=settings.DOMAIN_CITY)
        if domain.pk not in pk_set:
            instance.domains.add(domain)


class Address(models.Model):
    """Addresses of showrooms"""
    latitude = models.DecimalField(max_digits=10, decimal_places=6)
    longitude = models.DecimalField(max_digits=10, decimal_places=6)
    domain = models.ForeignKey(
        'Domain', null=True, blank=True, on_delete=models.SET_NULL)
    city = models.CharField(max_length=30)
    code = models.IntegerField()
    street = models.CharField(max_length=200)

    def save(self, *args, **kwargs):
        self.city = self.domain.city
        super(Address, self).save()

    def __str__(self):
        return f'{self.city} - {self.street}'

    class Meta:
        verbose_name_plural = "Addresses"


class Item(ImageAbstract):
    """Best items in showrooms"""
    name = models.CharField(max_length=200)
    title = models.CharField(max_length=200)
    description = models.TextField(max_length=800)
    link = models.URLField(max_length=200)
    is_best = models.BooleanField(default=False)
    showroom = models.ForeignKey(
        'Showroom', related_name='items', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class SocialMedia(models.Model):
    """General class for social media"""
    name = models.CharField(max_length=200)
    link = models.URLField(max_length=200)
    icon = models.ImageField(upload_to=get_path_icon)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Social media"


class ShowroomSocial(models.Model):
    """Social media of a showroom"""
    postfix = models.CharField(max_length=200)
    link = models.URLField(max_length=200)
    social = models.ForeignKey(
        'SocialMedia', null=True, related_name='social_media',
        on_delete=models.SET_NULL)
    showroom = models.ForeignKey('Showroom', related_name='showroom_social',
                                 on_delete=models.CASCADE)

    def save(self):
        if not self.postfix.endswith("/"):
            self.postfix += "/"
        self.link = self.social.link + self.postfix
        super(ShowroomSocial, self).save()

    def __str__(self):
        return self.postfix


class PhoneNumber(models.Model):
    """Phone number of a showroom"""
    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{1}\(?1?\d{2,4}\)?1?\d{2,3}\-?1?\d{2}\-?1?\d{'r'2}$',
        message="Phone number must be entered in the format: "
                "'+7(999)999-99-99'")
    phone_number = models.CharField(
        validators=[phone_regex], max_length=16, blank=True)
    showroom = models.ForeignKey(
        'Showroom', related_name='showroom_phone', null=True,
        on_delete=models.CASCADE)

    def __str__(self):
        return self.phone_number


User = get_user_model()


class Rating(models.Model):
    """Showroom rating"""
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    star = models.IntegerField(
        validators=[MaxValueValidator(5), MinValueValidator(1)], blank=True,
        null=True)
    showroom = models.ForeignKey(
        Showroom, on_delete=models.CASCADE, related_name='ratings')

    def __str__(self):
        return f"{self.star} - {self.showroom}"


class SavedShowroom(models.Model):
    """Saved showrooms for a user"""
    showroom = models.ForeignKey(
        Showroom, on_delete=models.CASCADE, related_name="saved")
    saved_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.showroom} - {self.saved_by}"


class ShowroomImage(models.Model):
    """Image for showroom detail page"""
    image = models.ImageField(
        upload_to=get_path_showroom_image,
        help_text=f"Required size: {settings.IMG_SIZE['showroom_detail']}",
        validators=[FileExtensionValidator(allowed_extensions=['png']),
                    ImageDimensionsValidator('showroom_detail')])
    image_thumbnail = models.ImageField(
        upload_to=get_path_showroom_image, editable=False)
    alt = models.CharField(max_length=200,
                           help_text='Alt for showroom image SEO')
    showroom = models.ForeignKey(
        Showroom, on_delete=models.CASCADE,
        related_name="showroom_detail_images")

    def save(self, *args, **kwargs):
        if (ShowroomImage.objects.filter(showroom=self.showroom).count() >
                settings.SHOWROOM_IMAGE_NUMBER):
            raise ValidationError(
                "More than " + str(settings.COMMENTS_IMAGE_NUMBER) + " images")
        else:
            super(ShowroomImage, self).save()
            width, height = settings.IMG_SIZE['showroom_thumbnail'].split('х')
            make_thumbnail(
                self.image_thumbnail, self.image, (int(width), int(height)))
            super(ShowroomImage, self).save()
