from common.service import AbstractPagination


class ShowroomPagination(AbstractPagination):
    """Pagination for all showroom lists (main page, category, subcategory)"""
    page_size = 9


class BestItemsPagination(AbstractPagination):
    """Pagination for best items"""
    page_size = 8


def get_path_icon(instance, file):
    """ Building path to a social media icon, format:
    (media)/social_icons/photo.jpg"""
    return f'social_icons/{file}'


def get_path_showroom_image(instance, file):
    return f'images/{instance.showroom.name}/detail_img/{file}'
