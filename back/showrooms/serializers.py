from rest_framework import serializers

from .models import (Showroom, Category, Rating, SavedShowroom, User, Item,
                     PhoneNumber, ShowroomSocial, SocialMedia, ShowroomImage, )
from common.serializers import RecursiveSerializer


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = "__all__"


class ItemSerializer(serializers.ModelSerializer):
    """For items and best items"""

    class Meta:
        model = Item
        fields = ("name", "title", "description", "link", 'grid_img', )


class PhoneNumberSerializer(serializers.ModelSerializer):
    """For shopwroom phone numbers"""

    class Meta:
        model = PhoneNumber
        fields = ("phone_number", )


class SocialMediaSerializer(serializers.ModelSerializer):
    """General social media serializer"""

    class Meta:
        model = SocialMedia
        fields = ("name", "icon", )


class ShowroomSocialSerializer(serializers.ModelSerializer):
    """Social media for a showroom"""
    social_name = serializers.ReadOnlyField(source='social.name')
    social_icon = serializers.ImageField(source='social.icon')

    class Meta:
        model = ShowroomSocial
        fields = ("link", "social_name", "social_icon", )


class ShowroomImageSerializer(serializers.ModelSerializer):
    """Images for showroom detail page"""

    class Meta:
        model = ShowroomImage
        fields = ("image", "image_thumbnail", )


class ShowroomsCustomFieldsSerializer(serializers.ModelSerializer):
    """Fields added to showroom in serializer. Abstract"""
    rating_user = serializers.BooleanField(default=False)
    user_star = serializers.IntegerField(
        min_value=1, max_value=5, default=None)
    average_star = serializers.DecimalField(
        default=0, max_digits=3, decimal_places=2)
    comments_number = serializers.IntegerField(default=0)
    saved_user = serializers.BooleanField(default=False)
    saved_number = serializers.IntegerField(default=0)
    coords = serializers.ListField()

    class Meta:
        abstract = True


class ShowroomDetailSerializer(ShowroomsCustomFieldsSerializer):
    """All showroom data for single showroom pages"""
    addresses = serializers.SlugRelatedField(
        slug_field='street', read_only=True, many=True)
    domains = serializers.SlugRelatedField(
        slug_field='city', read_only=True, many=True)
    showroom_phone = PhoneNumberSerializer(many=True)
    showroom_children = RecursiveSerializer(many=True)
    showroom_social = ShowroomSocialSerializer(many=True)
    showroom_detail_images = ShowroomImageSerializer(many=True)

    class Meta:
        model = Showroom
        exclude = ('is_draft', 'grid_img', 'recommend_img', )


class ShowroomListSerializer(ShowroomsCustomFieldsSerializer):
    """Short showroom data for pages wih multiple showrooms"""

    class Meta:
        model = Showroom
        fields = (
            'id', 'name', 'title', 'description', 'rating_user',
            'average_star', 'saved_number', 'saved_user', 'grid_img',
            'comments_number', 'user_star', 'slug', )


class ShowroomSearchSerializer(serializers.ModelSerializer):
    """Serializer for search hints"""

    class Meta:
        model = Showroom
        fields = ('id', 'name', )


class CategoryListSerializer(serializers.ModelSerializer):
    """Short category data for menu"""

    class Meta:
       model = Category
       fields = "__all__"


class CategoryRecommendationSerializer(serializers.ModelSerializer):
    """Short category data for recommendations"""

    class Meta:
        model = Category
        fields = ('id', 'name', 'title', 'recommend_img', 'slug', )


class CategoryDetailSerializer(serializers.ModelSerializer):
    """All category data for single category pages"""
    filter = serializers.SerializerMethodField()

    def get_filter(self, instance):
        items = [{"name": instance.name, "value": instance.id}]
        data = {
            "id": 2,
            "Placeholder": "Категория",
            "field": "categories__id",
            "items": items
        }
        return data

    class Meta:
        model = Category
        fields = (
            'id', 'detail_img', 'name', 'title', 'description',
            'filter', )


class RatingCreateSerializer(serializers.ModelSerializer):
    """Add showroom rating"""

    class Meta:
        model = Rating
        fields = ("star", "showroom", )

    def create(self, validated_data):
        rating, _ = Rating.objects.update_or_create(
            user=validated_data.get('user', None),
            showroom=validated_data.get('showroom', None),
            defaults={'star': validated_data.get("star")}
        )
        return rating


class SavedShowroomSerializer(serializers.ModelSerializer):
    """Add showroom to saved"""

    class Meta:
        model = SavedShowroom
        fields = ("showroom", )

    def create(self, validated_data):
        saved_showroom, _ = SavedShowroom.objects.update_or_create(
            saved_by=validated_data.get('saved_by', None),
            showroom=validated_data.get("showroom", None),
        )
        return saved_showroom


class ShowroomRecommendationSerializer(serializers.ModelSerializer):
    """For recommendations of showrooms"""

    class Meta:
        model = Showroom
        fields = ('id', 'name', 'title', 'recommend_img', 'slug', )


class ShowroomGeoDataSerializer(serializers.ModelSerializer):
    """Showroom geo data for Yandex maps"""
    coords = serializers.ListField()
    
    class Meta:
        model = Showroom
        fields = ('id', 'name', 'coords', )


class StructuredDataSerializer(serializers.ModelSerializer):
    """For structured showroom data"""
    addresses = serializers.SlugRelatedField(
        slug_field='street', read_only=True, many=True)

    class Meta:
        model = Showroom
