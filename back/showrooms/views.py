import random

from django.db.models import Q, F, OuterRef
from django.db.models.functions import JSONObject
from rest_framework import generics, viewsets, permissions, status
from rest_framework.decorators import api_view, action
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from django.conf import settings
from rest_framework.views import APIView
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters

from common.mixins import MixedPermission
from .models import (
    Showroom, Address, Category, Rating, SavedShowroom, Item, Domain, )
from common.permissions import IsAuthor
from common.classes import CategoryMPNode
from .serializers import (
    ShowroomListSerializer, ShowroomDetailSerializer, CategoryListSerializer,
    RatingCreateSerializer, SavedShowroomSerializer, ItemSerializer,
    CategoryDetailSerializer, CategoryRecommendationSerializer,
    ShowroomRecommendationSerializer, ShowroomSearchSerializer,
    ShowroomGeoDataSerializer)
from .service import ShowroomPagination, BestItemsPagination


class ShowroomViewSet(viewsets.ReadOnlyModelViewSet):
    """Showrooms single pade and main page"""
    filter_backends = [
        DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filter_fields = ['domains', 'categories', 'is_vintage', ]
    search_fields = [
        'name', 'description', 'categories__name', 'addresses__city', ]
    ordering_fields = ['name', 'time_published', 'ratings', ]
    pagination_class = ShowroomPagination

    def get_queryset(self):
        subquery = Address.objects.filter(
            showrooms_in_addresses__id=OuterRef("pk")
        ).annotate(
            data=JSONObject(
                latitude=F("latitude"),
                longitude=F("longitude"),
                address="street",
                domain="domain__city",
                domain_id="domain"
            )).values_list("data")
        return Showroom.objects.get_annotations(
            self.request.user.id, subquery).filter(is_draft=False)

    def get_serializer_class(self):
        if self.action == 'list':
            return ShowroomListSerializer
        elif self.action == 'retrieve':
            return ShowroomDetailSerializer
        elif self.action == 'search_showroom':
            return ShowroomSearchSerializer

    @action(detail=False)
    def search_showroom(self, request):
        search = request.query_params.get('search')
        showrooms = Showroom.objects.all().filter(is_draft=False).filter(
            Q(name__startswith=search) |
            Q(description__startswith=search) |
            Q(categories__name__startswith=search) |
            Q(addresses__city__startswith=search)
        ).distinct()
        page = self.paginate_queryset(showrooms)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(showrooms, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def get_geo_data(self, request):
        showrooms = self.get_queryset()
        serializer = ShowroomGeoDataSerializer(showrooms, many=True)
        return Response(serializer.data)


class CategoryMenuView(generics.ListAPIView):
    """List of categories and subcategories for menu"""
    serializer_class = CategoryListSerializer
    pagination_class = None

    def get_queryset(self):
        return Category.dump_bulk()

    def list(self, request):
        categories = self.get_queryset()
        return Response(categories)


class CategoryRecommendationsView(generics.ListAPIView):
    """List of categories for recommendations"""
    serializer_class = CategoryRecommendationSerializer

    def get_queryset(self):
        query_ids = list(Category.objects.all().values_list('id', flat=True))
        try:
            random_ids = random.sample(
                query_ids, settings.CATEGORY_RECOMMENDATIONS)
            return Category.objects.filter(id__in=random_ids)
        except ValueError:
            return Category.objects.all().order_by('?')


class CategoryDetailView(generics.RetrieveAPIView):
    """Category or subcategory page"""
    serializer_class = CategoryDetailSerializer

    def get_object(self):
        return Category.objects.get(id=self.kwargs['pk'])

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            # queryset just for schema generation metadata
            return Category.objects.none()


class BestItemsView(generics.ListAPIView):
    """Best items of the showroom"""
    serializer_class = ItemSerializer
    pagination_class = BestItemsPagination

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            # queryset just for schema generation metadata
            return Item.objects.none()
        return Item.objects.filter(
            showroom__id=self.kwargs['pk'], is_best=True)


class AddStarRatingViewSet(MixedPermission, viewsets.ModelViewSet):
    """Add showroom rating"""
    queryset = Rating.objects.all()
    serializer_class = RatingCreateSerializer
    permission_classes_by_action = {
        'post': [permissions.IsAuthenticated],
        'patch': [IsAuthor],
        'delete': [IsAuthor, permissions.IsAdminUser],
    }

    def get_object(self):
        obj = get_object_or_404(
            Rating,
            showroom=self.request.data['showroom'],
            user=self.request.user
        )
        self.check_object_permissions(self.request, obj)
        return obj

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class SaveShowroomViewSet(MixedPermission, viewsets.ModelViewSet):
    """Add showroom to saved"""
    queryset = SavedShowroom.objects.all()
    serializer_class = SavedShowroomSerializer
    permission_classes_by_action = {
        'post': [permissions.IsAuthenticated],
        'patch': [IsAuthor],
        'delete': [IsAuthor, permissions.IsAdminUser],
    }

    def get_object(self):
        obj = get_object_or_404(
            SavedShowroom,
            showroom=self.request.data['showroom'],
            saved_by=self.request.user
        )
        self.check_object_permissions(self.request, obj)
        return obj

    def perform_create(self, serializer):
        serializer.save(saved_by=self.request.user)


class GetFilterView(APIView):
    """Filters for main page"""
    def get_active_city(self, domain):
        if domain in self.request.get_host():
            return True
        return False

    def get_city_data(self):
        all_cities = Domain.objects.all().exclude(
            city=settings.DOMAIN_CITY
        ).values_list('id', 'city', 'domain')
        items = ({
            'name': i[1],
            'value': i[0],
            'active': self.get_active_city(i[2])} for i in all_cities)
        data = {
            "id": 1,
            'placeholder': 'Города',
            "field": 'domains',
            "items": items
        }
        return data

    def get_vintage_data(self):
        items = [
            {"name": "Винтаж", "value": 'true'},
            {"name": "Новое", "value": 'false'}]
        data = {
            "id": 3,
            'placeholder': 'Винтаж',
            "field": 'is_vintage',
            "items": items
        }
        return data

    def get_filter_data(self):
        return self.get_city_data(), self.get_vintage_data()

    def get(self, request):
        return Response(self.get_filter_data())


class ShowroomRecommendationsView(generics.ListAPIView):
    """Showrooms for recommendations"""
    serializer_class = ShowroomRecommendationSerializer

    def get_queryset(self):
        query_ids = list(Showroom.objects.all().values_list('id', flat=True))
        try:
            random_ids = random.sample(
                query_ids, settings.SHOWROOM_RECOMMENDATIONS)
            return Showroom.objects.filter(id__in=random_ids)
        except ValueError:
            return Showroom.objects.all().order_by('?')


class ShowroomStructuredData(APIView):
    """Structured data for showrooms"""

    def get_addresses_data(self, showroom):
        addresses_data = []
        for address in showroom.addresses.all():
            addresses_data_dict = {
                "@type": "ContactPoint",
                "streetAddress": address.street,
                "addressLocality": address.city,
                "postalCode": address.code,
                "addressCountry": "RU",
                "geo": {
                    "@type": "GeoCoordinates",
                    "latitude": address.latitude,
                    "longitude": address.longitude
                },
            }
            addresses_data.append(addresses_data_dict)
        return addresses_data

    def get_images_data(self, showroom):
        images_data = []
        if showroom.recommend_img:
            images_data.append(showroom.recommend_img.url)
        if showroom.grid_img:
            images_data.append(showroom.grid_img.url)
        return images_data

    def get_showroom_data(self, showroom, addresses_data):
        all_showroom_data = []
        phone_number = showroom.showroom_phone.all().first().phone_number
        for address in addresses_data:
            single_showroom_data = {
                "@context": "https://schema.org",
                "@type": "Store",
                "image": self.get_images_data(showroom),
                "name": showroom.name,
                "address": address,
                "url": showroom.link,
                "telephone": phone_number,
            }
            all_showroom_data.append(single_showroom_data)
        return all_showroom_data

    def get_structured_data(self, pk):
        showroom = Showroom.objects.get(id=pk)
        addresses_data = self.get_addresses_data(showroom)
        return self.get_showroom_data(showroom, addresses_data)

    def get(self, request, pk):
        return Response(self.get_structured_data(pk))


class SitemapDataView(APIView):
    """Gather data for sitemap.xml"""

    def get_showroom_slugs(self):
        showrooms_dict = {
            "showroom": Showroom.objects.all().values('id', 'slug')
        }
        return showrooms_dict

    def get_categories_slugs(self):
        categories_dict = {
            "category": CategoryMPNode.dump_bulk(Category)
        }
        return categories_dict

    def get(self, request):
        new_dict = dict(self.get_showroom_slugs(), **self.get_categories_slugs())
        return Response(new_dict)
