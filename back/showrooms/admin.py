from django.contrib import admin
from treebeard.admin import TreeAdmin
from treebeard.forms import movenodeform_factory

from .models import (
    Category, Domain, Showroom, Address, Item, SocialMedia, ShowroomSocial,
    PhoneNumber, Rating, SavedShowroom, ShowroomImage, )
from common.admin import AdminInlineWithPrefetchRelated, AdminWithPrefetchRelated


class CategoryAdmin(TreeAdmin):
    """"Admin for category"""
    form = movenodeform_factory(Category)
    prepopulated_fields = {"title": ('name', )}


class DomainAdmin(admin.ModelAdmin):
    """"Admin for domain"""
    readonly_fields = ('domain', )
    ordering = ('domain', )


class AddressAdmin(admin.ModelAdmin):
    """"Admin for address"""
    readonly_fields = ('city', )


class AddressTabularInline(AdminInlineWithPrefetchRelated):
    """"Inline for address in showroom"""
    model = Showroom.addresses.through
    list_prefetch_related = (
        'showrooms_in_addresses',
    )
    extra = 0
    # readonly_fields = ('address', )
    #
    # def get_queryset(self, request):
    #     qs = super(AddressTabularInline, self).get_queryset(request)
    #     return qs.select_related('address')


class ItemAdmin(admin.ModelAdmin):
    """Admin for items"""
    model = Item
    prepopulated_fields = {"title": ('name', )}


class ItemTabularInline(admin.TabularInline):
    """"Inline for best item in showroom"""
    model = Item
    readonly_fields = ('name', 'link', 'grid_img', 'recommend_img')
    exclude = ('title', 'description', 'alt')


class ShowroomSocialTabularInline(AdminInlineWithPrefetchRelated):
    """"Inline for social media in showroom"""
    model = ShowroomSocial
    readonly_fields = ('link', )


class PhoneNumberTabularInline(admin.TabularInline):
    """Inline for phone number in showroom"""
    model = PhoneNumber


class RatingTabularInline(admin.TabularInline):
    """Inline for rating in showroom"""
    model = Rating
    readonly_fields = ("star", "showroom", "user", )


class SavedShowroomTabularInline(admin.TabularInline):
    """Inline for saved showroom (which user saved this)"""
    model = SavedShowroom
    readonly_fields = ("saved_by", )


class ShowroomImageTabularInline(admin.TabularInline):
    """Inline for showroom detail images"""
    model = ShowroomImage


class ShowroomAdmin(AdminWithPrefetchRelated):
    """"Admin for showroom"""
    inlines = [
        AddressTabularInline, ItemTabularInline, ShowroomSocialTabularInline,
        PhoneNumberTabularInline, RatingTabularInline,
        SavedShowroomTabularInline, ShowroomImageTabularInline, ]
    model = Showroom
    exclude = ('addresses',)
    readonly_fields = ('time_published', )
    prepopulated_fields = {"title": ('name', )}

    # list_prefetch_related = (
    #     'addresses',
    # )
    list_prefetch_related = (
        'showroom_social',
    )

    def save_related(self, request, form, formsets, change):
        super().save_related(request, form, formsets, change)
        categories_list = []
        for category in form.instance.categories.filter(depth__gt=1):
            if category.get_root() not in categories_list:
                categories_list.append(category.get_root())
        form.instance.categories.add(*categories_list)


admin.site.register(Category, CategoryAdmin)
admin.site.register(Domain, DomainAdmin)
admin.site.register(SocialMedia)
admin.site.register(Address, AddressAdmin)
admin.site.register(Showroom, ShowroomAdmin)
admin.site.register(SavedShowroom)
admin.site.register(Item, ItemAdmin)


