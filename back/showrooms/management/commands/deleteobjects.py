from django.core.management.base import BaseCommand

from showrooms.models import Category, Domain, Address, Showroom


class Command(BaseCommand):
    """"Delete random objects from database"""
    help = 'Delete objects'

    def handle(self, *args, **options):
        Category.objects.filter(name__contains="Category_").delete()
        Category.objects.filter(name__contains="Subcategory_").delete()
        Domain.objects.filter(city__contains="_Test").delete()
        Address.objects.filter(street__contains="Test address ").delete()
        Showroom.objects.filter(name__contains="Showroom_").delete()
