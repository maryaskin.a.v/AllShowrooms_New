# import random
#
# from django.core.exceptions import ObjectDoesNotExist
# from django.core.management.base import BaseCommand
# from django.conf import settings
#
# from showrooms.models import Category, Domain, Address, Showroom
#
# dict_for_cities = {
#     # Format: city, subdomain prefix, latitude range, longitude range
#     "Moscow_Test": ["moscow_test", [55.574716, 55.911094], [37.372262, 37.853237]],
#     "Saint-Petersburg_Test": ["spb_test", [59.845, 60.084719], [30.224134, 30.516197]],
#     "Novosibirsk_Test": ["nsk_test", [55.006879, 55.017708], [82.906232, 82.932362]],
# }
#
#
# class Command(BaseCommand):
#     """"Fill the tables with random objects"""
#     help = 'Create objects'
#
#     is_domain = Domain.objects.filter(city__contains="_Test").exists()
#     is_category = Category.objects.filter(name__contains="Category_").exists()
#     is_subcategory = Category.objects.filter(name__contains="Subcategory_").exists()
#     is_address = Address.objects.filter(street__contains="Test address ").exists()
#
#     def create_categories(self, categories_number):
#         for i in range(categories_number):
#             Category.objects.get_or_create(name="Category_" + str(i))
#             self.is_category = True
#
#     def create_subcategories(self, subcategories_number):
#         if self.is_category:
#             for i in range(subcategories_number):
#                 subcategory_name = "Subcategory_" + str(i)
#                 try:
#                     Category.objects.get(name=subcategory_name)
#                 except ObjectDoesNotExist:
#                     category = random.choice(Category.objects.filter(name__contains="Category_",
#                                                                      ))
#                     Category.objects.create(
#                         name=subcategory_name,
#                     )
#                     self.is_subcategory = True
#
#     def create_domains(self):
#         try:
#             Domain.objects.get(city=settings.DOMAIN_CITY)
#         except ObjectDoesNotExist:
#             Domain.objects.create(city=settings.DOMAIN_CITY)
#
#         for i in dict_for_cities.keys():
#             try:
#                 Domain.objects.get(prefix__contains=dict_for_cities[i][0])
#             except ObjectDoesNotExist:
#                 Domain.objects.create(city=i, prefix=dict_for_cities[i][0])
#                 self.is_domain = True
#
#     def create_addresses(self, addresses_number):
#         existing_addresses = Address.objects.filter(street__contains="Test address ").count()
#         if self.is_domain and existing_addresses < addresses_number:
#             for i in range(addresses_number - existing_addresses):
#                 try:
#                     Address.objects.get(street__contains="Test address " + str(i))
#                 except ObjectDoesNotExist:
#                     random_latitude, random_longitude, domain = self._randomize_coords()
#                     self._get_or_create_address(random_latitude, random_longitude, i, domain)
#                     self.is_address = True
#
#     def create_showrooms(self, showrooms_number):
#         existing_showrooms = Showroom.objects.filter(name__contains="Showroom_").count()
#         is_exists = bool(self.is_domain and self.is_address and self.is_subcategory)
#
#         if is_exists and existing_showrooms < showrooms_number:
#             for i in range(showrooms_number - existing_showrooms):
#                 try:
#                     Showroom.objects.get(name__contains="Showroom_" + str(i))
#                 except ObjectDoesNotExist:
#                     new_room = "Showroom_" + str(i)
#                     show_room = Showroom.objects.create(
#                         name=new_room, title=new_room, description=new_room, is_draft=False,
#                     )
#
#                     subcategories = self._get_showroom_subcategories()
#                     addresses, domains = self._get_showroom_addresses()
#
#                     show_room.categories.set(subcategories)
#                     show_room.domains.set(domains)
#                     show_room.addresses.set(addresses)
#                     show_room.save()
#
#     def _randomize_coords(self):
#         # City data list: subdomain prefix, latitude range (list), longitude range (list)
#         domain = self._get_domain()
#         random_city_data = dict_for_cities.get(domain.city)
#         min_latitude = random_city_data[1][0] * 1000000
#         max_latitude = random_city_data[1][1] * 1000000
#         random_latitude = random.randint(min_latitude, max_latitude) / 1000000
#         min_longitude = random_city_data[2][0] * 1000000
#         max_longitude = random_city_data[2][1] * 1000000
#         random_longitude = random.randint(min_longitude, max_longitude) / 1000000
#         return random_latitude, random_longitude, domain
#
#     @staticmethod
#     def _get_domain():
#         random_city = str(random.choice([*dict_for_cities]))
#         return Domain.objects.get(city=random_city)
#
#     @staticmethod
#     def _get_or_create_address(random_latitude, random_longitude, i, domain):
#         Address.objects.get_or_create(
#             latitude=random_latitude,
#             longitude=random_longitude,
#             domain=domain,
#             code=str(i),
#             street="Test address " + str(i)
#         )
#
#     @staticmethod
#     def _get_showroom_subcategories():
#         return random.choices(Category.objects.filter(name__contains="Subcategory_",
#                                                       parent__isnull=False))
#
#     @staticmethod
#     def _get_showroom_addresses():
#         addresses = random.choices(Address.objects.filter(street__contains="Test address "))
#         cities = [address.city for address in addresses]
#         domains = Domain.objects.filter(city__in=cities)
#         return addresses, domains
#
#     def handle(self, *args, **options):
#         self.create_categories(10)
#         self.create_subcategories(20)
#         self.create_domains()
#         self.create_addresses(10)
#         self.create_showrooms(20)
