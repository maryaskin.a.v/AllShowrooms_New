from django.core.exceptions import ValidationError
from django.conf import settings

from common.service import AbstractPagination


def validate_comment_image(image):
    """Check file size (comment img)"""
    size_limit = settings.COMMENTS_IMAGE_SIZE
    actual_size = image.size

    if actual_size > size_limit:
        raise ValidationError("File should be smaller than " + str(size_limit))


class CommentsPagination(AbstractPagination):
    """Pagination for comments"""
    page_size = 10

