from django.contrib import admin

from .models import Comment, CommentImage
from treebeard.forms import movenodeform_factory


class CommentImageTabularInline(admin.TabularInline):
    """Inline for comment images"""
    model = CommentImage


class CommentAdmin(admin.ModelAdmin):
    """"Admin for showroom"""
    form = movenodeform_factory(Comment)
    inlines = [CommentImageTabularInline, ]
    readonly_fields = ('time_published', )


admin.site.register(Comment, CommentAdmin)
