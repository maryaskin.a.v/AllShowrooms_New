from django.utils import timezone
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.core.validators import FileExtensionValidator
from django.db import models
from django.conf import settings

from .service import validate_comment_image
from common.service import get_user_path, make_thumbnail
from selections.models import Selection
from showrooms.models import Showroom
from treebeard.mp_tree import MP_Node


class Comment(MP_Node):
    """Comments for showroom, selections"""
    text = models.TextField("Comment", max_length=5000)
    time_published = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(
        get_user_model(), on_delete=models.SET_NULL, blank=True, null=True,
        related_name="user_comments")
    user_like = models.ManyToManyField(
        get_user_model(), blank=True, related_name="users_like")
    user_dislike = models.ManyToManyField(
        get_user_model(), blank=True, related_name="users_dislike")
    showroom = models.ForeignKey(
        Showroom, on_delete=models.SET_NULL, null=True, blank=True,
        related_name="showroom_comments")
    selection = models.ForeignKey(
        Selection, on_delete=models.SET_NULL, null=True, blank=True,
        related_name="selection_comments")

    FK_list = ['showroom', 'selection', ]

    def save(self, *args, **kwargs):
        self.save_time()
        super(Comment, self).save(*args, **kwargs)

    def save_time(self):
        if not self.time_published:
            self.time_published = timezone.now()

    def __str__(self):
        return f"{self.user} - {self.id} - {self.time_published}"

    def clean(self):
        usage = 0
        for field in self._meta.fields:
            if (field.name in self.FK_list and
                    getattr(self, field.name) is not None):
                usage += 1
        if usage > 1:
            raise ValidationError('Comment has more that one model')
        elif usage == 0:
            raise ValidationError('Comment has no model')

    def user_directory_path(self):
        return 'user_{0}/{1}'.format(self.user.id, self.image.name)


def get_fk_models_and_fields():
    models_and_fields = {}
    for field in Comment._meta.get_fields():
        if field.get_internal_type() == 'ForeignKey':
            models_and_fields[field.remote_field.model.__name__] = field.name
    return models_and_fields


class CommentImage(models.Model):
    """Image for comments"""
    comment = models.ForeignKey(
        Comment, on_delete=models.CASCADE, related_name="comment_images")
    image = models.ImageField(
        upload_to=get_user_path,
        validators=[
            FileExtensionValidator(allowed_extensions=['jpg', 'png']),
            validate_comment_image])
    image_thumbnail = models.ImageField(
        upload_to=get_user_path, editable=False)

    def save(self, *args, **kwargs):
        if (CommentImage.objects.filter(comment=self.comment).count() >
                settings.COMMENTS_IMAGE_NUMBER):
            raise ValidationError(
                "More than " + str(settings.COMMENTS_IMAGE_NUMBER) + " images")
        else:
            super(CommentImage, self).save()
            width, height = settings.IMG_SIZE['comment_thumbnail'].split('х')
            make_thumbnail(
                self.image_thumbnail, self.image, (int(width), int(height)))
            super(CommentImage, self).save()
