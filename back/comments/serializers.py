from rest_framework import serializers

from .models import Comment, CommentImage
from common.serializers import RecursiveSerializer


class CommentImageSerializer(serializers.ModelSerializer):
    """Comment images"""

    class Meta:
        model = CommentImage
        fields = ('image', 'image_thumbnail', )


class CommentCreateImageSerializer(serializers.ModelSerializer):
    """Comment images"""

    class Meta:
        model = CommentImage
        fields = ('image', )


class CommentsListSerializer(serializers.ModelSerializer):
    """All comments to an object"""
    children = RecursiveSerializer(many=True)
    comment_images = CommentImageSerializer(many=True)
    user_name = serializers.ReadOnlyField(source='user.username')
    avatar = serializers.ImageField(source='user.avatar', required=False)
    likes = serializers.IntegerField(default=0)
    dislikes = serializers.IntegerField(default=0)

    class Meta:
        model = Comment
        fields = (
            'id', 'user_name', 'avatar', 'text', 'time_published',
            'showroom', 'selection', 'comment_images', 'children', 'likes',
            'dislikes', )


class CreateShowroomCommentSerializer(serializers.ModelSerializer):
    """Add comment to an object"""
    uploaded_images = serializers.ImageField(
        required=False, write_only=True, default=None)
    comment_images = CommentImageSerializer(
        many=True, required=False, read_only=True)
    parent = serializers.IntegerField(default=None, required=False)

    def create(self, validated_data):
        del validated_data['uploaded_images']
        parent = validated_data.pop('parent')
        if parent is None:
            comment = Comment.add_root(**validated_data)
        else:
            node = Comment.objects.get(id=parent)
            comment = node.add_child(**validated_data)
        images = self.context['uploaded_images']
        for image in images:
            CommentImage.objects.create(comment=comment, image=image)
        return comment

    class Meta:
        model = Comment
        fields = (
            "id", "text", "uploaded_images", "showroom", "comment_images", "parent")
