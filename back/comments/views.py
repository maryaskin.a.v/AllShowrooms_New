from rest_framework import viewsets, permissions, generics, status, parsers
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Comment
from .serializers import (
    CommentsListSerializer, CreateShowroomCommentSerializer, )
from .service import CommentsPagination
from common.mixins import MixedPermission
from common.permissions import IsAuthor
from common.classes import CommentMPNode
from profiles.models import Profile


class ShowroomCommentsListView(generics.ListAPIView):
    """Comments list for a showroom"""
    serializer_class = CommentsListSerializer
    pagination_class = CommentsPagination

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            return self.get_paginated_response(page)
        return Response(queryset)

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            # queryset just for schema generation metadata
            return Comment.objects.none()
        return CommentMPNode.dump_bulk(Comment)


class SelectionCommentsListView(generics.ListAPIView):
    """Comments list for a selection"""
    serializer_class = CommentsListSerializer

    def get_queryset(self):
        return Comment.objects.filter(
            selection__isnull=False, parent__isnull=True,
            selection__id=self.kwargs['pk']).prefetch_related('children')


class CreateShowroomCommentViewSet(MixedPermission, viewsets.ModelViewSet):
    """Add comment to a showroom"""
    queryset = Comment.objects.filter(showroom__isnull=False)
    serializer_class = CreateShowroomCommentSerializer
    parser_classes = (parsers.MultiPartParser, )
    permission_classes_by_action = {
        'post': [permissions.IsAuthenticated],
        'patch': [IsAuthor],
        'delete': [IsAuthor, permissions.IsAdminUser],
    }

    def create(self, request, *args, **kwargs):
        images = request.FILES.getlist('uploaded_images', None)
        context = {'uploaded_images': images}
        serializer = self.get_serializer(data=request.data, context=context)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class LikeCommentView(MixedPermission, viewsets.ModelViewSet):
    """Like comment"""
    permission_classes_by_action = {
        'post': [permissions.IsAuthenticated],
        'patch': [IsAuthor],
        'delete': [IsAuthor],
    }

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            # queryset just for schema generation metadata
            return Comment.objects.none()

    @action(detail=True, methods=['post'])
    def like(self, request, pk):
        comment = Comment.objects.get(id=pk)
        data = {
            'user_dislikes': 0,
            'user_likes': 0,
        }
        if request.user in comment.user_like.all():
            comment.user_like.remove(Profile.objects.get(id=request.user.id))
        else:
            if request.user in comment.user_dislike.all():
                comment.user_dislike.remove(Profile.objects.get(
                    id=request.user.id))
            data['user_likes'] = 1
            comment.user_like.add(Profile.objects.get(
                id=request.user.id))
        comment.save()
        return Response(data, status=201)

    @action(detail=True, methods=['post'])
    def dislike(self, request, pk):
        comment = Comment.objects.get(id=pk)
        data = {
            'user_dislikes': 0,
            'user_likes': 0,
        }
        if request.user in comment.user_dislike.all():
            comment.user_dislike.remove(Profile.objects.get(
                id=request.user.id))
        else:
            if request.user in comment.user_like.all():
                comment.user_like.remove(Profile.objects.get(
                    id=request.user.id))
            data['user_dislikes'] = 1
            comment.user_dislike.add(Profile.objects.get(
                id=request.user.id))
        comment.save()
        return Response(data, status=201)



