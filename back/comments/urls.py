from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views


urlpatterns = format_suffix_patterns([
    path('like/<int:pk>/', views.LikeCommentView.as_view({'post': 'like'})),
    path('dislike/<int:pk>/', views.LikeCommentView.as_view({
        'post': 'dislike'})),
    path('showroom/<int:pk>/', views.ShowroomCommentsListView.as_view()),
    path('showroom/', views.CreateShowroomCommentViewSet.as_view({
        'post': 'create'})),
])
