from django.core.validators import FileExtensionValidator
from django.db import models
from django.conf import settings

from .service import get_path_image, ImageDimensionsValidator


class ImageAbstract(models.Model):
    """
    Layout images for showrooms, categories, subcategories, selections.

    Grid_image - image in list views, example:
    https://gyazo.com/47608bd2cbee56178adb7ac4fc8bc223

    Recommend_image - image in related/recommended endpoints, example:
    https://gyazo.com/161d0e3bf019e437bea494d35487afb0
    """

    grid_img = models.ImageField(
        upload_to=get_path_image, null=True, blank=True,
        help_text=f"Required size: {settings.IMG_SIZE['grid']}",
        validators=[
            FileExtensionValidator(allowed_extensions=['png']),
            ImageDimensionsValidator('grid')])
    recommend_img = models.ImageField(
        upload_to=get_path_image, null=True, blank=True,
        help_text=f"Required size: {settings.IMG_SIZE['recommend']}",
        validators=[
            FileExtensionValidator(allowed_extensions=['png']),
            ImageDimensionsValidator('recommend')])
    alt = models.CharField(max_length=200,
                           help_text='Alt for grid_img, recommend_img')

    class Meta:
        abstract = True

