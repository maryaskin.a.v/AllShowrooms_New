from treebeard.mp_tree import MP_Node, get_result_class
from django.db.models import Count, F


class MyMPNode(MP_Node):
    fields = ()

    @classmethod
    def get_fields(cls):
        return cls.fields

    @classmethod
    def dump_bulk(cls, my_model, parent=None, keep_ids=True):
        """Dumps a tree branch to a python data structure."""

        model = get_result_class(my_model)
        qset = cls.get_qset(model, my_model)

        if parent:
            qset = qset.filter(path__startswith=parent.path)
        ret, lnk = [], {}
        pk_field = model._meta.pk.attname

        for pyobj in qset:
            # django's serializer stores the attributes in 'fields'
            fields = pyobj.copy()
            path = fields['path']
            depth = int(len(path) / model.steplen)
            # this will be useless in load_bulk
            del fields['depth']
            del fields['path']
            del fields['numchild']
            if pk_field in fields:
                # this happens immediately after a load_bulk
                del fields[pk_field]
            # newobj = {'data': fields}
            newobj = fields
            if keep_ids:
                newobj['id'] = pyobj['id']

            if (not parent and depth == 1) or \
                    (parent and len(path) == len(parent.path)):
                ret.append(newobj)
            else:
                parentpath = model._get_basepath(path, depth - 1)
                parentobj = lnk[parentpath]
                if 'children' not in parentobj:
                    parentobj['children'] = []
                parentobj['children'].append(newobj)
            lnk[path] = newobj
        return ret


class CommentMPNode(MyMPNode):
    """Nodes that allow using values lists and annotations. Works for comments"""

    @classmethod
    def get_qset(cls, model, my_model):
        list_of_fields = [f.name for f in model._meta.get_fields()]
        return model._get_serializable_model().objects.select_related(
            'showroom',
            'selection'
        ).values(
            *list_of_fields,
            comments_images=F('comment_images__image'),
            likes=Count('user_like'),
            dislikes=Count('user_dislike'),
            username=F('user__username'),
        )


class CategoryMPNode(MyMPNode):
    """Node for categories in sitemap"""
    fields = ('id', 'slug', 'path', 'depth', 'numchild')
    
    @classmethod
    def get_qset(cls, model, my_model):
        return model._get_serializable_model().objects.values(
            *cls.get_fields()
        )
