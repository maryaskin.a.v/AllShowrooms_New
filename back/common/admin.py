from django.contrib import admin
from django.contrib.admin.options import BaseModelAdmin
from django.db.models.constants import LOOKUP_SEP


class AdminBaseWithPrefetchRelated(BaseModelAdmin):
    """
    Admin Base using list_prefetch_related for get_queryset related fields
    """
    list_prefetch_related = []

    def get_queryset(self, request):
        return super(AdminBaseWithPrefetchRelated, self) \
            .get_queryset(request) \
            .prefetch_related(*self.list_prefetch_related)

    def form_apply_prefetch_related(self, form):
        for related_field in self.list_prefetch_related:
            splitted = related_field.split(LOOKUP_SEP)

            if len(splitted) > 1:
                field = splitted[0]
                related = LOOKUP_SEP.join(splitted[1:])
                form.base_fields[field].queryset = form.base_fields[field] \
                    .queryset.prefetch_related(related)


class AdminInlineWithPrefetchRelated(admin.TabularInline,
                                     AdminBaseWithPrefetchRelated):
    """
    Admin Inline using list_prefetch_related for get_queryset and get_formset
    related fields
    """

    def get_formset(self, request, obj=None, **kwargs):
        formset = super(AdminInlineWithPrefetchRelated, self) \
            .get_formset(request, obj, **kwargs)

        self.form_apply_prefetch_related(formset.form)

        return formset


class AdminWithPrefetchRelated(admin.ModelAdmin, AdminBaseWithPrefetchRelated):
    """
    Admin using list_prefetch_related for get_queryset and get_form related
    fields
    """

    def get_form(self, request, obj=None, **kwargs):
        form = super(AdminWithPrefetchRelated, self).get_form(request, obj,
                                                              **kwargs)

        self.form_apply_prefetch_related(form)

        return form
