import random
import string

from django.core.files.images import get_image_dimensions
from django.core.exceptions import ValidationError
from django.conf import settings
from django.db.models.fields.files import ImageFieldFile
from django.utils.deconstruct import deconstructible
from django.core.files.base import ContentFile

from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from PIL import Image
from io import BytesIO
import os.path


def get_slug():
    """Buliding a default slug"""
    return ''.join(random.choices(string.ascii_letters + string.digits, k=6))


def get_path_image(instance, file):
    """ Building path to an image, format: (media)/images/showroom/photo.jpg"""
    return f'images/{instance.name}/{file}'


def get_user_path(instance, file):
    """ Building path to user uploaded image, format:
    (media)/users_uploaded/user_1/photo.jpg"""
    return f'users_uploaded/user_{instance.comment.user.id}/{file}'


def get_user_path_thumbnail(instance, file):
    """ Building path to user uploaded image, format:
    (media)/users_uploaded/user_1/thumbnail/photo.jpg"""
    return f'users_uploaded/thumbnail/user_{instance.comment.user.id}/{file}'


def make_thumbnail(dst_image_field, src_image_field, size):
    """Make thumbnails for images"""
    img = Image.open(src_image_field)
    img.thumbnail(size)
    _, dst_ext = os.path.splitext(src_image_field.name)
    dst_ext = dst_ext.lower()
    if dst_ext in ['.jpg', '.jpeg']:
        filetype = 'JPEG'
    elif dst_ext == '.gif':
        filetype = 'GIF'
    elif dst_ext == '.png':
        filetype = 'PNG'
    else:
        raise RuntimeError('Unrecognized file type of "%s"' % dst_ext)
    dst_bytes = BytesIO()
    img.save(dst_bytes, filetype)
    dst_bytes.seek(0)
    dst_image_field.save(
        get_user_path_thumbnail, ContentFile(dst_bytes.read()), save=False)
    dst_bytes.close()


@deconstructible
class ImageDimensionsValidator:
    """Validate image dimensions"""

    def __init__(self, name: str):
        self.element_name = name

    def __call__(self, value: ImageFieldFile):
        valid_image_dimensions = self.check_image_dimensions(value)
        if not valid_image_dimensions:
            raise ValidationError('Wrong image size')

    def check_image_dimensions(self, value: ImageFieldFile):
        dimensions = settings.IMG_SIZE[self.element_name].split('х')
        width, height = get_image_dimensions(value.file)
        if str(width) != dimensions[0] or str(height) != dimensions[1]:
            return False
        return True


class AbstractPagination(PageNumberPagination):
    """Base pagination for all pages"""

    def paginate_queryset(self, queryset, request, view=None):
        self.count_pages = self.django_paginator_class(queryset, self.page_size)
        return super().paginate_queryset(queryset, request, view)

    def get_paginated_response(self, data):
        return Response({
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'count': self.page.paginator.count,
            'count_pages': self.count_pages.num_pages,
            'results': data,
        })

    class Meta:
        abstract = True
