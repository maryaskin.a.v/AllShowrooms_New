from rest_framework import permissions


class IsAuthor(permissions.IsAuthenticated):
    """Permission if user created the instance"""
    def has_object_permission(self, request, view, obj):
        return obj.user == request.user
