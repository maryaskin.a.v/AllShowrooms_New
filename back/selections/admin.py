from django.contrib import admin

from .models import Selection, Criterion


class SelectionAdmin(admin.ModelAdmin):
    """Admin for selections"""
    model = Selection
    prepopulated_fields = {"title": ('name', )}
    readonly_fields = ('time_published', )


admin.site.register(Criterion)
admin.site.register(Selection, SelectionAdmin)

