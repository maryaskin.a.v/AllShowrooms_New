from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = format_suffix_patterns([
    path('', views.SelectionViewSet.as_view({'get': 'list'})),
    path('<int:pk>/', views.SelectionViewSet.as_view({'get': 'retrieve'})),
    path('showrooms-items/<int:pk>/',
         views.SelectionShowroomsItemsView.as_view()),
    path('main-header/', views.MainPageSelectionsView.as_view()),
    path('recommendations/', views.SelectionsRecommendationsView.as_view()),
    path('criteria/', views.CriteriaView.as_view()),
    path('sitemap/', views.SitemapDataView.as_view()),
])
