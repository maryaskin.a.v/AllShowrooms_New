from rest_framework import serializers

from .models import Selection, Criterion
from showrooms.serializers import ShowroomListSerializer, ItemSerializer


class CriterionSerializer(serializers.ModelSerializer):
    """Criterion serializer"""

    class Meta:
        model = Criterion
        fields = '__all__'


class SelectionDetailSerializer(serializers.ModelSerializer):
    """Details for single selection page"""
    showrooms = ShowroomListSerializer(many=True)
    items = ItemSerializer(many=True)

    class Meta:
        model = Selection
        exclude = (
            'is_draft', 'time_published', 'criteria', 'grid_img',
            'recommend_img', 'to_main_page', 'slug', )


class SelectionListSerializer(serializers.ModelSerializer):
    """For a page with multiple selections"""

    class Meta:
        model = Selection
        fields = ('id', 'name', 'title', 'grid_img', 'slug', )


class SelectionHeaderListSerializer(serializers.ModelSerializer):
    """For main page header"""

    class Meta:
        model = Selection
        fields = ('id', 'name', 'title', 'header_img', 'slug', )


class SelectionRecommendationSerializer(serializers.ModelSerializer):
    """For recommendations of selections"""

    class Meta:
        model = Selection
        fields = ('id', 'name', 'title', 'recommend_img', 'slug', )
