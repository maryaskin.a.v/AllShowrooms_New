import random

from rest_framework import viewsets, generics
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response
from rest_framework import filters
from django.conf import settings
from rest_framework.views import APIView

from showrooms.serializers import ShowroomListSerializer, ItemSerializer
from showrooms.models import Showroom, Item
from .serializers import (
    SelectionListSerializer, SelectionDetailSerializer,
    SelectionHeaderListSerializer, SelectionRecommendationSerializer,
    CriterionSerializer)
from .models import Selection, Criterion
from .service import SelectionsPagination


class SelectionViewSet(viewsets.ReadOnlyModelViewSet):
    """Selections list and details of a single selection"""
    filter_backends = [DjangoFilterBackend, filters.SearchFilter,
                       filters.OrderingFilter]
    filter_fields = ['criteria', ]
    pagination_class = SelectionsPagination

    def get_queryset(self):
        return Selection.objects.filter(is_draft=False).order_by(
            'time_published')

    def get_serializer_class(self):
        if self.action == 'list':
            return SelectionListSerializer
        if self.action == 'retrieve':
            return SelectionDetailSerializer


class SelectionShowroomsItemsView(generics.ListAPIView):
    """Showrooms and items for selection"""
    serializer_class_showroom = ShowroomListSerializer
    serializer_class_item = ItemSerializer

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            # queryset just for schema generation metadata
            return Showroom.objects.none()

    def get_queryset_showroom(self):
        return Showroom.objects.get_annotations(self.request.user.id).filter(
            showroom_selections__id__icontains=self.kwargs['pk'],
            is_draft=False)

    def get_queryset_item(self):
        return Item.objects.filter(
            item_selections__id__icontains=self.kwargs['pk'])

    def list(self, request, *args, **kwargs):
        showroom = self.serializer_class_showroom(
            self.get_queryset_showroom(), many=True)
        item = self.serializer_class_item(self.get_queryset_item(), many=True)
        return Response({
            "SHOWROOMS": showroom.data,
            "ITEMS": item.data
        })


class MainPageSelectionsView(generics.ListAPIView):
    """Selections for main page header"""
    serializer_class = SelectionHeaderListSerializer

    def get_queryset(self):
        return Selection.objects.filter(
            is_draft=False, to_main_page=True).order_by('time_published')


class SelectionsRecommendationsView(generics.ListAPIView):
    """Selections for recommendations"""
    serializer_class = SelectionRecommendationSerializer

    def get_queryset(self):
        query_ids = list(Selection.objects.all().values_list('id', flat=True))
        try:
            random_ids = random.sample(
                query_ids, settings.SELECTION_RECOMMENDATIONS)
            return Selection.objects.filter(id__in=random_ids)
        except ValueError:
            return Selection.objects.all().order_by('?')


class CriteriaView(generics.ListAPIView):
    """All selections criteria"""
    serializer_class = CriterionSerializer

    def get_queryset(self):
        return Criterion.objects.all()


class SitemapDataView(APIView):
    """Gather data for sitemap.xml"""

    def get_selection_slugs(self):
        showrooms_dict = {
            "selections": Selection.objects.all().values('id', 'slug')
        }
        return showrooms_dict

    def get(self, request):
        return Response(self.get_selection_slugs())
