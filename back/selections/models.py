from datetime import datetime

from django.db import models
from django.conf import settings
from django.core.validators import FileExtensionValidator
from django.core.exceptions import ValidationError

from showrooms.models import Showroom, Item
from common.models import ImageAbstract
from common.service import get_path_image, ImageDimensionsValidator


class Criterion(models.Model):
    """Criteria to filter selections"""
    name = models.CharField(max_length=200, unique=True)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return f"{self.name}"


class Selection(ImageAbstract):
    """Collection of showrooms and items"""
    detail_img = models.ImageField(
        upload_to=get_path_image,
        help_text=f"Required size: {settings.IMG_SIZE['selection_detail']}",
        validators=[FileExtensionValidator(allowed_extensions=['png']),
                    ImageDimensionsValidator('selection_detail')])
    header_img = models.ImageField(
        upload_to=get_path_image,
        help_text=f"Required size: {settings.IMG_SIZE['header']}",
        validators=[FileExtensionValidator(allowed_extensions=['png']),
                    ImageDimensionsValidator('header')])
    name = models.CharField(max_length=200,
                            help_text='Selection name in Russian')
    title = models.CharField(max_length=200, help_text='Title for SEO')
    alt = models.CharField(max_length=200,
                           help_text='Alt for detail_img, header_img')
    slug = models.SlugField(unique=True)
    description = models.TextField(max_length=800)
    time_published = models.DateTimeField()
    is_draft = models.BooleanField(default=True)
    to_main_page = models.BooleanField(default=False)
    showrooms = models.ManyToManyField(
        Showroom, blank=True, related_name="showroom_selections")
    items = models.ManyToManyField(
        Item, blank=True, related_name="item_selections")
    criteria = models.ManyToManyField(Criterion)

    def save(self, *args, **kwargs):
        self.check_for_header()
        self.save_time_published(*args, **kwargs)

    def save_time_published(self, *args, **kwargs):
        if not self.time_published and not self.is_draft:
            self.time_published = datetime.now()
        super(Selection, self).save(*args, **kwargs)

    def check_for_header(self):
        if not self.is_draft and self.to_main_page and not self.header_img:
            raise ValidationError("Needs a header image!")

    def __str__(self):
        return self.name

