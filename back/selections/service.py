from common.service import AbstractPagination


class SelectionsPagination(AbstractPagination):
    """Pagination for selections list and filtration"""
    page_size = 12

