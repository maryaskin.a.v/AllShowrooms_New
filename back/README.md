### Старт и билд

    docker-compose up --build

### Старт

    docker-compose up

### Создать супер юзера
- Запустить проект через докер
- Открыть еще одну консоль


    docker exec -it back_web_1 bash
    python manage.py createsuperuser


### Выгрузка данных

    ./manage.py dumpdata > data.json

### Загрузка данных

    ./manage.py loaddata data.json