from djoser.compat import get_user_email
from djoser.views import UserViewSet
from djoser.conf import settings
from rest_framework import generics, permissions, status, viewsets
from rest_framework.response import Response
from rest_framework.decorators import action, api_view
from rest_framework.exceptions import ValidationError
from django_filters.rest_framework import DjangoFilterBackend

from common.mixins import MixedPermission
from common.permissions import IsAuthor
from .models import Profile, ProfileCode
from .serializers import ProfileSerializer, ShowroomCommentsSerializer, \
    VerifyCodeSerializer, \
    NewEmailSerializer
from comments.models import Comment
from showrooms.serializers import (
    ShowroomListSerializer, ShowroomRecommendationSerializer)
from showrooms.models import Showroom
from .service import ProfileCommentsPagination, generate_code, email_code, \
    create_code_verified_text, code_active


def create_profile_code(profile_id, code, email):
    ProfileCode.objects.create(profile_id=profile_id, code=code, email=email)
    # TODO: как удалить модель через некоторое время? Чтобы код не был бесконечным


def update_profile_code(profile_id, new_code):
    profile_code_instance = ProfileCode.objects.get(profile_id=profile_id)
    profile_code_instance.code = new_code
    profile_code_instance.save()


def verify_serializer(request, new_serializer):
    serializer = new_serializer(data=request.data)
    serializer.is_valid(raise_exception=True)


class ProfileViewSet(MixedPermission, UserViewSet):
    """User profile base"""
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    lookup_field = 'pk'
    permission_classes_by_action = {
        'get': [IsAuthor, permissions.IsAdminUser],
        'patch': [IsAuthor],
        'delete': [IsAuthor],
        'reset_password': settings.PERMISSIONS.password_reset
    }

    @action(["post"], detail=False)
    def reset_password(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.get_user()
        if user:
            context = {"user": user}
            settings.EMAIL.password_reset(self.request, context).send(
                [get_user_email(user)])
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            raise ValidationError(detail="User does not exist")

# TODO: в настройки PASSWORD_CHANGED_EMAIL_CONFIRMATION = true
    def perform_update(self, serializer):
        serializer.save()


@api_view(['POST'])
def send_reset_code(request):
    """Insert new email and send code to an old one"""
    verify_serializer(request, NewEmailSerializer)
    if request.data["email"] == request.user.email:
        raise ValidationError(detail="Same email")
    code = generate_code()
    create_profile_code(request.user.id, code, request.data["email"])
    email_code(code, request.user.email)
    return Response({"Success": create_code_verified_text(request.user.email)},
                    status=200)


@api_view(['POST'])
def verify_code(request):
    """Verify emailed code and send a new code to a new email"""
    verify_serializer(request, VerifyCodeSerializer)
    profile_code_exists = ProfileCode.objects.filter(
        profile_id=request.user.id,
        code=request.data[
            "code"]).exists()
    if profile_code_exists:
        profile_code = ProfileCode.objects.get(profile_id=request.user.id)
        if code_active(profile_code.time_created):
            new_code = generate_code()
            new_email = profile_code.email
            update_profile_code(request.user.id, new_code)
            email_code(new_code, new_email)
            return Response({"Success": create_code_verified_text(new_email)},
                            status=200)
        else:
            return Response({"Failed": "Code expired. Request a new one"},
                            status=400)
    return Response(status=400)


@api_view(['POST'])
def verify_new_email(request):
    """Verify new email with new code"""
    verify_serializer(request, VerifyCodeSerializer)
    profile_code_exists = ProfileCode.objects.filter(
        profile_id=request.user.id,
        code=request.data[
            "code"]).exists()
    if profile_code_exists:
        profile_code_instance = ProfileCode.objects.get(
            profile_id=request.user.id)
        if code_active(profile_code_instance.time_created):
            profile_instance = Profile.objects.get(id=request.user.id)
            profile_instance.email = profile_code_instance.email
            profile_instance.save()
            profile_code_instance.delete()
            return Response(status=200)
        else:
            return Response({"Failed": "Code expired. Request a new one"},
                            status=400)
    return Response(status=400)


class ShowroomCommentsView(generics.ListAPIView):
    """Comments by this user for a showroom"""
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['showroom', ]
    serializer_class = ShowroomCommentsSerializer
    pagination_class = ProfileCommentsPagination

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            # queryset just for schema generation metadata
            return Comment.objects.none()
        return Comment.objects.filter(
            user=self.request.user,
            parent__isnull=True,
            showroom=self.kwargs.get('pk')
        ).prefetch_related('children').order_by('time_published')


class CommentedShowroomsView(generics.ListAPIView):
    """Showrooms commented by this user"""
    serializer_class = ShowroomRecommendationSerializer
    pagination_class = ProfileCommentsPagination

    def get_queryset(self):
        return Showroom.objects.filter(
            showroom_comments__user=self.request.user).distinct('id')


class ProfileSavedView(generics.ListAPIView):
    """Showrooms saved by user"""
    serializer_class = ShowroomListSerializer

    def get_queryset(self):
        return Showroom.objects.filter(saved__saved_by=self.request.user)
