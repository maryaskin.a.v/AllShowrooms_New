from abc import ABC

from rest_framework import serializers
from djoser.serializers import UserSerializer
from djoser.serializers import TokenCreateSerializer
from djoser.conf import settings
from django.contrib.auth import authenticate
from rest_framework import status
from rest_framework.exceptions import APIException

from .models import Profile, ProfileCode
from comments.models import Comment
from comments.serializers import CommentImageSerializer
from common.serializers import RecursiveSerializer
from showrooms.serializers import (
    ShowroomRecommendationSerializer, ShowroomListSerializer)


class ProfileSerializer(UserSerializer):
    """Basic profile settings"""

    def __init__(self, *args, **kwargs):
        kwargs['partial'] = True
        super(ProfileSerializer, self).__init__(*args, **kwargs)

    class Meta(UserSerializer.Meta):
        model = Profile
        fields = (
            'id', 'username', 'email', "news_confirm", "comments_confirm",
            "avatar", )
        read_only_fields = ()


class ShowroomCommentsSerializer(serializers.ModelSerializer):
    """Serializer for showroom comments"""
    children = RecursiveSerializer(many=True)
    comment_images = CommentImageSerializer(many=True)
    showroom_data = ShowroomRecommendationSerializer(source='showroom')

    class Meta:
        model = Comment
        fields = (
            'text', 'time_published', 'parent', 'comment_images', 'children',
            'showroom_data', )


class ProfileCommentsSerializer(serializers.ModelSerializer):
    """Comments of this profile"""
    user_comments = ShowroomCommentsSerializer(many=True)

    class Meta:
        model = Profile
        fields = ('user_comments', )


class CustomTokenCreateSerializer(TokenCreateSerializer):
    """Override for an appropriate error message (inactive user login)"""

    def validate(self, attrs):
        password = attrs.get("password")
        params = {settings.LOGIN_FIELD: attrs.get(settings.LOGIN_FIELD)}
        self.user = authenticate(**params, password=password)
        if not self.user:
            self.user = Profile.objects.filter(**params).first()
            if self.user and not self.user.check_password(password):
                self.fail("invalid_credentials")
        if self.user and not self.user.is_active:
            raise UserInactiveError
        elif self.user and self.user.is_active:
            return attrs
        self.fail("invalid_credentials")


class UserInactiveError(APIException):
    """Error if inactive user is trying to login"""
    status_code = status.HTTP_403_FORBIDDEN
    default_detail = 'user is not active'


class VerifyCodeSerializer(serializers.Serializer):
    """Serializer to verify emailed code"""
    code = serializers.CharField(max_length=6)


class NewEmailSerializer(serializers.Serializer):
    """Serializer to enter new email"""
    email = serializers.EmailField()
