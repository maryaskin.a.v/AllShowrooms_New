from django.contrib import admin

from .models import Profile, ProfileCode


class ProfileCodeAdmin(admin.ModelAdmin):
    """"Admin for profile codes"""
    readonly_fields = ('time_created',)


admin.site.register(Profile)
admin.site.register(ProfileCode, ProfileCodeAdmin)
