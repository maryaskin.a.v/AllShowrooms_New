import random
import string

from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.conf import settings
from django.utils import timezone
from django.core.mail import send_mail

from common.service import AbstractPagination


def get_path_avatar(instance, file):
    """ Building path to a profile, format: (media)/avatars/user/photo.jpg"""
    return f'avatars/{instance.username}/{file}'


def validate_avatar_image(image):
    """Check file size (comment img)"""
    size_limit = settings.AVATAR_IMAGE_SIZE
    actual_size = image.size

    if actual_size > size_limit:
        raise ValidationError("File should be smaller than " + str(size_limit))


def generate_code():
    """Building 6 character code to verify email change"""
    return ''.join(random.choices(string.ascii_letters + string.digits, k=6))


def email_code(code, email):
    """Sending a code to an email to verify email change"""
    send_mail("Код для смены почты на allshowrooms.ru",
              code, settings.EMAIL_HOST_USER, [email], )


def create_code_verified_text(email):
    """Creating a success text when verifying emailed codes"""
    return "Check your email " + str(email) + " for verification " \
                                              "code"


def code_active(created_time):
    code_lifetime = timezone.now() - created_time
    code_lifetime_in_hours = code_lifetime.total_seconds() / 3600
    if code_lifetime_in_hours < settings.CODE_LIFETIME:
        return True
    return False


class ProfileCommentsPagination(AbstractPagination):
    """Pagination for comments in profile"""
    page_size = 10
