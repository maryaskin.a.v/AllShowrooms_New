from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views


urlpatterns = format_suffix_patterns([
    path('<int:pk>/', views.ProfileViewSet.as_view({'get': 'retrieve',
                                                    'patch': 'update',
                                                    'delete': 'destroy',
                                                    })),
    path('reset_password/', views.ProfileViewSet.as_view({
        'post': 'reset_password'})),
    path("showrooms/", views.CommentedShowroomsView.as_view()),
    path("showrooms/<int:pk>/comments/", views.ShowroomCommentsView.as_view()),
    path("saved/", views.ProfileSavedView.as_view()),
    path("send_reset_code/", views.send_reset_code),
    path("verify_code/", views.verify_code),
    path("verify_new_email/", views.verify_new_email),
])
