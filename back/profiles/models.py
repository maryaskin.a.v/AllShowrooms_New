from django.core.validators import FileExtensionValidator
from django.db import models
from django.contrib.auth.models import AbstractUser

from .service import get_path_avatar, validate_avatar_image


class Profile(AbstractUser):
    """Profile model replacing the default user"""
    email = models.EmailField("email address", unique=True)
    news_confirm = models.BooleanField(default=True)
    comments_confirm = models.BooleanField(default=True)
    avatar = models.ImageField(
        upload_to=get_path_avatar, null=True, blank=True,
        validators=[FileExtensionValidator(allowed_extensions=['jpg', 'png']),
                    validate_avatar_image])
    REQUIRED_FIELDS = ["email", "password"]

    def __str__(self):
        return self.username


class ProfileCode(models.Model):
    """Profile id + code to sent to the profile's email"""
    profile_id = models.IntegerField()
    code = models.CharField(max_length=6)
    email = models.EmailField()
    time_created = models.DateTimeField(auto_now_add=True, null=True)
